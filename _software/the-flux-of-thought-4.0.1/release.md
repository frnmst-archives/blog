---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: the-flux-of-thought
software_version: 4.0.1
software_version_raw: 000004.000000.000001
release_timestamp: 2021-09-20 13:51:31
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Added deprecation notice.
- Added fixed width for all images (300px).
- Updated git hooks.
