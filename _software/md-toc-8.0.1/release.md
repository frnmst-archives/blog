---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.0.1
software_version_raw: 000008.000000.000001
release_timestamp: 2021-08-20 16:09:54
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Fixed

- Cmark-specific code is now more adherent to the original.
- License references are now more accurate.
- The PyPI wheel is now made reproducible.
- Updated email.

### Changed

- Cmark-specific code has been moved to a separate Python module: `cmark.py`.

### Added

- New git hooks have been added in the pre-commit file.
- Examples for unit tests of Cmark 0.30 have been checked with the existing examples.
