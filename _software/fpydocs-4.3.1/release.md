---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpydocs
software_name_python_module: fpydocs
software_version: 4.3.1
software_version_raw: 000004.000003.000001
release_timestamp: 2022-11-06 22:23:34
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Added

- Add deprecation notice

### Fixed

- Fix URLs
- Fix errors in a couple of instructions
- Removed irrelevant command in Makefile
