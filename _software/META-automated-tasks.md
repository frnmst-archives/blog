---
meta: true
software_name: automated-tasks
archived: false
canonical_repository_url: https://software.franco.net.eu.org/frnmst/automated-tasks
codeberg_repository_url: https://codeberg.org/frnmst/automated-tasks
framagit_repository_url: https://framagit.org/frnmst/automated-tasks
github_repository_url:
documentation_url: https://docs.franco.net.eu.org/automated-tasks
documentation_ref: docs.franco.net.eu.org/automated-tasks
has_changelog: true
---
