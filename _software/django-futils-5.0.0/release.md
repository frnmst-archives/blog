---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: django-futils
software_name_python_module: django_futils
software_version: 5.0.0
software_version_raw: 000005.000000.000000
release_timestamp: 2021-12-10 10:57:06
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Fixed

- Imported fixes from [fpydocs](https://software.franco.net.eu.org/frnmst/fpydocs).
- Updated copyright headers.
- Updated URLs.

### Added

- Use of Redis to improve performance.
- Added PyPI mirror variable in the Dockerfile.
- Added use of docker registry variable in the `docker-compose.yml.dist` file.
