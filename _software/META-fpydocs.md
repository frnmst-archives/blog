---
meta: true
software_name: fpydocs
archived: true
canonical_repository_url: https://software.franco.net.eu.org/frnmst-archives/fpydocs
codeberg_repository_url: https://codeberg.org/frnmst-archives/fpydocs
framagit_repository_url: https://framagit.org/frnmst-archives/fpydocs
github_repository_url:
documentation_url: https://docs.franco.net.eu.org/ftutorials
documentation_ref: docs.franco.net.eu.org/ftutorials
has_changelog: true
---
