---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: md-toc
software_name_python_module: md_toc
software_version: 7.2.0
software_version_raw: 000007.000002.000000
release_timestamp: 2021-04-10 15:20:59
is_on_pypi: false
has_changelog: false
signing_public_key: pgp_pubkey_since_2019.txt
---
