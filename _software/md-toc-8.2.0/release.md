---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.2.0
software_version_raw: 000008.000002.000000
release_timestamp: 2023-08-08 16:25:11
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Fixed

- Definetly closed
  [issue #36: *Consider Mentioning Existence of GitHub's Built-In Markdown Table of Contents*](https://github.com/frnmst/md-toc/issues/36).
  Added more projects for feature comparisons.
- [Issue #38: *Invalid Cross-device Link*](https://github.com/frnmst/md-toc/issues/38)
  declared officially closed for inactivity (no recent reports).

### Added

- Using the new `--diff` option enables md-toc to return `128` if the existing
  TOC in a file is different from the newly generated one, instead of a normal
  `0` value. Some CI systems don't allow modifying files in-place when running
  pre-commit.

  By returning something different than 0, the pre-commit pipeline fails so the
  user gets notified that the TOC needs to be changed.
  See [issue #40: *There are no diff unless we specify `-p`*](https://github.com/frnmst/md-toc/issues/40).

Note: GitHub Flavored Markdown is still at version 0.29. md-toc does its best
to support both GFM 0.29 and cmark 0.30.
