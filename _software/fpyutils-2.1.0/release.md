---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpyutils
software_name_python_module: fpyutils
software_version: 2.1.0
software_version_raw: 000002.000001.000000
release_timestamp: 2021-11-26 22:55:22
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Added

- New pre-commit hooks.

### Changed

- The `fpyutils.filelines.insert_string_at_line` function now uses the `newline` argument.
  This means that an input file is entirly re-written with the selected newline
  instead of using the pre-exsising one for the non-inserted lines. This
  prevents files from having mixed newlines.
- The `fpyutils.filelines.insert_string_at_line` function now uses `os.linesep` instead
  of `\n` as newline.

### Fixed

- Passing the `newline` argument to the `atomicwrites` method
  in `fpyutils.filelines.insert_string_at_line` function fixes [md-toc issue #33](https://github.com/frnmst/md-toc/issues/33).
- Imported fixes from fpydocs
- Fixed email, URLs and copyright headers
