---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: automated-tasks
software_name_python_module: automated_tasks
software_version: 10.0.0
software_version_raw: 000010.000000.000000
release_timestamp: 2021-03-04 19:08:43
is_on_pypi: false
has_changelog: false
signing_public_key: pgp_pubkey_since_2019.txt
---
