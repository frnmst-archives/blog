---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.1.5
software_version_raw: 000008.000001.000005
release_timestamp: 2022-10-28 11:22:10
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Added

- Added support for ignoring emphasis in HTML point braces (`< ... >`).
  See [issue #25](https://github.com/frnmst/md-toc/issues/25).
- Added some benchmarks and a benchmark script. These benchmarks are not
  reproducible: random strings are generated each run. There are two purposes
  for these benchmarks:
  - see how md-toc performance changes with each release
  - detect bugs (runtime errors, exceptions especially)
    that are not addressed by the tests file

### Fixed

- Fixed some docstring documentation. See [issue #39](https://github.com/frnmst/md-toc/issues/39).
- Fixed variable typing using mypy git hook.

Note: GitHub Flavored Markdown is still at version 0.29. md-toc does its best to
      support both GFM 0.29 and cmark 0.30.
