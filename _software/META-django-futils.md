---
meta: true
software_name: django-futils
archived: false
canonical_repository_url: https://software.franco.net.eu.org/frnmst/django-futils
codeberg_repository_url: https://codeberg.org/frnmst/django-futils
framagit_repository_url: https://framagit.org/frnmst/django-futils
github_repository_url:
documentation_url: https://docs.franco.net.eu.org/django-futils
documentation_ref: docs.franco.net.eu.org/django-futils
has_changelog: true
---
