---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: licheck
software_name_python_module: licheck
software_version: 0.0.1
software_version_raw: 000000.000000.000001
release_timestamp: 2021-08-06 08:58:09
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Added

- First release.
