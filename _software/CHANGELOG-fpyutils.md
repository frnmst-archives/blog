---
layout: changelog
enable_markdown: true
software_name: fpyutils
title: fpyutils changelog
excerpt: none
---

## Plan for version 4.0.2

### Fixed

- [__✓__ 0005010](https://software.franco.net.eu.org/frnmst/fpyutils/commit/0005010)
  Improve documentation for the API so it is more readable: use `autosummary`
  and `automodule`.
