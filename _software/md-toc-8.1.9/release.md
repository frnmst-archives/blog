---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.1.9
software_version_raw: 000008.000001.000009
release_timestamp: 2023-02-10 17:08:57
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Moved some cmark constants to their appropriate files so as to mirror the
  upstream C code.

### Fixed

- Added an API example in the README file to write the TOC in place.
- Improve and update md-toc's pre-commit hook documentation.
- Continuing to fix [issue #25](https://github.com/frnmst/md-toc/issues/25).
- Improved md-toc version history table: split versions to improve
  readability.

Note: GitHub Flavored Markdown is still at version 0.29. md-toc does its best
to support both GFM 0.29 and cmark 0.30.
