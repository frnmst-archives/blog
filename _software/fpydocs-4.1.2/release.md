---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpydocs
software_name_python_module: fpydocs
software_version: 4.1.2
software_version_raw: 000004.000001.000002
release_timestamp: 2021-10-18 20:55:43
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Relaxed Python package requirements in the `Pipfile`.

### Added

- Added new options in the `Makefile` blueprint.

### Removed

- Removed `[requires]` section from the `Pipfile`.
