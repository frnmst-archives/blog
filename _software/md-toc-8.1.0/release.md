---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.1.0
software_version_raw: 000008.000001.000000
release_timestamp: 2021-11-29 22:30:24
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Fixed

- New line output is now handled correctly.
  See [issue 33](https://github.com/frnmst/md-toc/issues/33).
- Updated copyright headers.
- Imported fixes from [fpydocs](https://blog.franco.net.eu.org/software/#fpydocs).
- Metadata fixes in the setup.py file.
