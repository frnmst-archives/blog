---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpyutils
software_name_python_module: fpyutils
software_version: 2.0.1
software_version_raw: 000002.000000.000001
release_timestamp: 2021-07-30
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Added

- New pre-commit hooks.

### Changed

- Changed assertions to exceptions.
- Updated package dependencies.
- Updated URLs.
