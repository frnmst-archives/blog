---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fattura-elettronica-reader
software_name_python_module: fattura_elettronica_reader
software_version: 2.0.4
software_version_raw: 000002.000000.000004
release_timestamp: 2021-04-03 21:28:13
is_on_pypi: false
has_changelog: false
signing_public_key: pgp_pubkey_since_2019.txt
---
