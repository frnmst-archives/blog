---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: automated-tasks
software_name_python_module: automated_tasks
software_version: 11.2.0
software_version_raw: 000011.000002.000000
release_timestamp: 2021-08-24 21:21:22
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Fixed

- Updated URLs in documentation.

### Added

- New git hooks have been added in the pre-commit file.
- Added a note concerning physical partition in the documentation for `qvm.py`.
- Binary options for `youtube_dl.py`.
