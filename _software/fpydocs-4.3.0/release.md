---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpydocs
software_name_python_module: fpydocs
software_version: 4.3.0
software_version_raw: 000004.000003.000000
release_timestamp: 2022-02-20 22:42:12
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Updated copyright years.

### Fixed

- Fixed contributing common page with workflow changes of the last year:
  - use of `pipenv`
  - simplified instructions and wording
- Fixed `SOURCE_DATE_EPOCH` variable in `Makefile`: added an option to hide the
  git signature that would prevent the correct output for the variable if not
  hidden.

### Added

- Added git remote instructions for AUR repositories: the AUR repositories
  are not used as submodules of
  [frnmst-aur-packages-mirror](https://software.franco.net.eu.org/frnmst-archives/frnmst-aur-packages-mirror),
  which is now deprecated. Content is simply pushed on multiple remotes.
