---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.0.0
software_version_raw: 000008.000000.000000
release_timestamp: 2021-05-28 17:52:41
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Translated functions directly from cmark to treat emphasis.
- `md_toc.api.get_md_header` and `md_toc.api.get_atx_heading` now support
  multiple lines.
- Updated documentation.
- Updated tests.
- Updated package dependencies.

### Added

- Newline string argument choice from the CLI.
- Added these exceptions:
  - `md_toc.exceptions.StringCannotContainNewlines`
  - `md_toc.exceptions.CannotTreatUnicodeString`

### Removed

- Removal of these functions:
  - `md_toc.api.get_generic_fdr_indices`
  - `md_toc.api.get_fdr_indices`
  - `md_toc.api.can_open_emphasis`
  - `md_toc.api.can_close_emphasis`
  - `md_toc.api.get_nearest_list_id`
