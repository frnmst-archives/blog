---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: automated-tasks
software_name_python_module:  automated_tasks
software_version: 8.0.1
software_version_raw: 000008.000000.000001
release_timestamp: 2020-12-01 16:11:28
is_on_pypi: false
has_changelog: false
signing_public_key: pgp_pubkey_since_2019.txt
---
