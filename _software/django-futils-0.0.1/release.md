---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: django-futils
software_name_python_module: django_futils
software_version: 0.0.1
software_version_raw: 000000.000000.000001
release_timestamp: 2020-11-11 15:01:59
is_on_pypi: false
has_changelog: false
signing_public_key: pgp_pubkey_since_2019.txt
---
