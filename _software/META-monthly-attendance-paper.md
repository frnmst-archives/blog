---
meta: true
software_name: monthly-attendance-paper
archived: true
canonical_repository_url: https://software.franco.net.eu.org/frnmst-archives/monthly-attendance-paper
codeberg_repository_url: https://codeberg.org/frnmst-archives/monthly-attendance-paper
framagit_repository_url: https://framagit.org/frnmst-archives/monthly-attendance-paper
github_repository_url:
documentation_url: https://software.franco.net.eu.org/frnmst-archives/monthly-attendance-paper#monthly-attendance-paper
documentation_ref: software.franco.net.eu.org/frnmst-archives/monthly-attendance-paper#monthly-attendance-paper
has_changelog: false
---
