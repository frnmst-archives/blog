---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: docker-debian-postgis-django
software_name_python_module: docker_debian_postgis_django
software_version: 1.0.0
software_version_raw: 000001.000000.000000
release_timestamp: 2021-01-27 18:40:58
is_on_pypi: false
has_changelog: false
signing_public_key: pgp_pubkey_since_2019.txt
---
