---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: qvm
software_version: 1.0.5
software_version_raw: 000001.000000.000005
release_timestamp: 2021-01-03 16:00:52
is_on_pypi: false
has_changelog: false
signing_public_key: pgp_pubkey_since_2019.txt
---
