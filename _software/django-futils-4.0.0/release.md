---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: django-futils
software_name_python_module: django_futils
software_version: 4.0.0
software_version_raw: 000004.000000.000000
release_timestamp: 2021-09-01 22:17:27
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Fixed

- Updated email.
- Fixed documentation concerning database backup and restore.
- Updated URLs.
- Fixed Dockerfile using linter.

### Changed

- The `DJANGO_ENV` environment variable has been moved directly
  into the init command because Jenkins is not able to read it from docker-compose directly.

### Added

- New git hooks have been added in the pre-commit file.
- A `Jenkinsfile` declarative pipeline file has been added instead of using
  the `ci.sh` script from
  [docker-debian-postgis-django](https://software.franco.net.eu.org/frnmst/docker-debian-postgis-django).

### Removed

- Removed old environment variables.
