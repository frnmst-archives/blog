---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: automated-tasks
software_name_python_module: automated_tasks
software_version: 13.0.1
software_version_raw: 000013.000000.000001
release_timestamp: 2022-02-21 11:37:48
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Updated `archive_invoice_files.py` script to support
  [fattura-elettronica-reader 3.0.0](https://software.franco.net.eu.org/frnmst/fattura-elettronica-reader/src/tag/3.0.0).
- Updated documentation:
  - Added and updated package dependencies for the
    - [`build_python_packages.py`](https://docs.franco.net.eu.org/automated-tasks/scripts.html#build-python-packages-py)
    - [`archive_invoice_files.py`](https://docs.franco.net.eu.org/automated-tasks/scripts.html#archive-invoice-files-py)
    scripts.
- Updated copyright years.

### Fixed

- Fixed typo in `Pipfile`.
