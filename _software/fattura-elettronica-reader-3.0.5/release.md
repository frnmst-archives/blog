---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fattura-elettronica-reader
software_name_python_module: fattura_elettronica_reader
software_version: 3.0.5
software_version_raw: 000003.000000.000005
release_timestamp: 2023-03-05 16:02:23
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Fixed

- Corrected file write in the `patch_invoice_schema_file` function.
- Fixed pre-commit hooks.
