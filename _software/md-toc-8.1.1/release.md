---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.1.1
software_version_raw: 000008.000001.000001
release_timestamp: 2022-01-27 17:22:24
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Fixed

- Fixed [issue 30](https://github.com/frnmst/md-toc/issues/30): added full support for
  CommonMark 0.30.

  Note:

  - GitHub Flavored Markdown is still at version 0.29. md-toc does its best to
    support both GFM 0.29 and cmark 0.30.
  - Previously missing implementations for the markdown emphasis removal have not
    been added with this release.

- Improved adherence to upstream cmark code.
- Added missing values in constants.
- Imported fixes from [fpydocs](https://blog.franco.net.eu.org/software/#fpydocs).
- Updated copyright headers.
