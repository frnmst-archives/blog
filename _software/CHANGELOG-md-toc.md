---
layout: changelog
enable_markdown: true
software_name: md-toc
title: md-toc changelog
excerpt: none
---

## Plan for 9.0.1

### Fixed

- [Issue #25: *Links not working when some styling is used in the header on GitHub*](https://github.com/frnmst/md-toc/issues/25):
  must be completed before this version.

## Plan for 9.0.0

See the 9.x GIT branch:

- [canonical repository](https://software.franco.net.eu.org/frnmst/md-toc/src/branch/9.x)
- [Codeberg](https://codeberg.org/frnmst/md-toc/src/branch/9.x)
- [Framagit](https://framagit.org/frnmst/md-toc/-/tree/9.x?ref_type=heads)
- [GitHub](https://github.com/frnmst/md-toc/tree/9.x)

### Fixed

- [__✓__ c8a6402](https://software.franco.net.eu.org/frnmst/md-toc/commit/c8a6402)

  Improve documentation for the API so it is more readable: use `autosummary`
  and `automodule`.
- [__✓__ 40f9231](https://software.franco.net.eu.org/frnmst/md-toc/commit/40f9231)

  Remove NULL bytes before passing the anchor link to the `remove_emphasis`
  cmark function. Without this change, md-toc fails with this and other similar
  inputs. More tests are needed to determine if the generated TOCs still work.

  This is the output of `hexyl a.md`:

  ```
  ┌────────┬─────────────────────────┬─────────────────────────┬────────┬────────┐
  │00000000│ 23 20 61 62 63 64 65 66 ┊ 67 68 69 6a 6b 6c 6d 6e │# abcdef┊ghijklmn│
  │00000010│ 6f 70 71 72 73 74 75 76 ┊ 77 78 79 7a 41 42 43 44 │opqrstuv┊wxyzABCD│
  │00000020│ 45 46 47 48 49 4a 4b 4c ┊ 4d 4e 4f 50 51 52 53 54 │EFGHIJKL┊MNOPQRST│
  │00000030│ 55 56 57 58 59 5a 60 00 ┊ 00 00 0a                │UVWXYZ`0┊00_     │
  └────────┴─────────────────────────┴─────────────────────────┴────────┴────────┘
  ```

  ```shell
  cat -e a.md
  # abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`^@^@^@$
  ```

  Result:

  ```shell
  python3 -m md_toc github a.md

    Traceback (most recent call last):
      File "./md-toc/md_toc/__main__.py", line 36, in main
        result = args.func(args)
                 ^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cli.py", line 79, in write_toc
        toc_struct = build_multiple_tocs(
                     ^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/api.py", line 483, in build_multiple_tocs
        return [
               ^
      File "./md-toc/md_toc/api.py", line 484, in <listcomp>
        build_toc(
      File "./md-toc/md_toc/api.py", line 325, in build_toc
        headers: list[types.Header] = get_md_header(
                                      ^^^^^^^^^^^^^^
      File "./md-toc/md_toc/api.py", line 1320, in get_md_header
        return [
               ^
      File "./md-toc/md_toc/api.py", line 1328, in <listcomp>
        build_anchor_link(
      File "./md-toc/md_toc/api.py", line 906, in build_anchor_link
        header_text_trimmed = remove_emphasis(header_text_trimmed, parser)
                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/api.py", line 836, in remove_emphasis
        ignore: list[range] = inlines_c._cmark_cmark_parse_inlines(
                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 2184, in _cmark_cmark_parse_inlines
        while not _cmark_is_eof(subj) and _cmark_parse_inline(
                                          ^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 2117, in _cmark_parse_inline
        new_inl = _cmark_handle_backticks(subj, options)
                  ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 639, in _cmark_handle_backticks
        openticks: _cmarkCmarkChunk = _cmark_take_while(subj, '_cmark_isbacktick')
                                      ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 497, in _cmark_take_while
        while _cmark_take_while_loop_condition(subj, '_cmark_isbacktick'):
              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 482, in _cmark_take_while_loop_condition
        c = _cmark_peek_char(subj)
            ^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 430, in _cmark_peek_char
        raise ValueError
    ValueError
  ```

- [__✓__ 40f9231](https://software.franco.net.eu.org/frnmst/md-toc/commit/40f9231)

  Add more checks in the `md_toc.cmark.inlines_c._cmark_subject_find_special_char`
  function to avoid overflows. More tests are needed to determine if the
  generated TOCs still work.

  This is the output of `hexyl a.md`:

  ```
  ┌────────┬─────────────────────────┬─────────────────────────┬────────┬────────┐
  │00000000│ 23 20 61 62 63 64 65 66 ┊ 67 68 69 6a 6b 6c 6d 6e │# abcdef┊ghijklmn│
  │00000010│ 6f 70 71 72 73 74 75 76 ┊ 77 78 79 7a 41 42 43 44 │opqrstuv┊wxyzABCD│
  │00000020│ 45 46 47 48 49 4a 4b 4c ┊ 4d 4e 4f 50 51 52 53 54 │EFGHIJKL┊MNOPQRST│
  │00000030│ 55 56 57 58 59 5a c5 8c ┊ 61 62 63 64 65 66 67 68 │UVWXYZ××┊abcdefgh│
  │00000040│ 69 6a 6b 6c 6d 6e 6f 70 ┊ 71 72 73 74 75 76 77 78 │ijklmnop┊qrstuvwx│
  │00000050│ 79 7a 41 42 43 44 45 46 ┊ 47 48 49 4a 4b 4c 4d 4e │yzABCDEF┊GHIJKLMN│
  │00000060│ 4f 50 51 52 53 54 55 56 ┊ 57 58 59 5a 0a          │OPQRSTUV┊WXYZ_   │
  └────────┴─────────────────────────┴─────────────────────────┴────────┴────────┘
  ```

  ```shell
  cat -e a.md
  # abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZM-EM-^LabcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$
  ```

  Result:

  ```shell
  python3 -m md_toc github a.md

     Traceback (most recent call last):
       File "./md-toc/md_toc/__main__.py", line 36, in main
        result = args.func(args)
                 ^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cli.py", line 79, in write_toc
        toc_struct = build_multiple_tocs(
                     ^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/api.py", line 483, in build_multiple_tocs
        return [
               ^
      File "./md-toc/md_toc/api.py", line 484, in <listcomp>
        build_toc(
      File "./md-toc/md_toc/api.py", line 325, in build_toc
        headers: list[types.Header] = get_md_header(
                                      ^^^^^^^^^^^^^^
      File "./md-toc/md_toc/api.py", line 1324, in get_md_header
        return [
               ^
      File "./md-toc/md_toc/api.py", line 1332, in <listcomp>
        build_anchor_link(
      File "./md-toc/md_toc/api.py", line 910, in build_anchor_link
        header_text_trimmed = remove_emphasis(header_text_trimmed, parser)
                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/api.py", line 840, in remove_emphasis
        ignore: list[range] = inlines_c._cmark_cmark_parse_inlines(
                              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 2187, in _cmark_cmark_parse_inlines
        while not _cmark_is_eof(subj) and _cmark_parse_inline(
                                          ^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 2152, in _cmark_parse_inline
        endpos = _cmark_subject_find_special_char(subj, options)
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
      File "./md-toc/md_toc/cmark/inlines_c.py", line 2090, in _cmark_subject_find_special_char
        if SPECIAL_CHARS[ord(subj.input.data[n])] == 1:
           ~~~~~~~~~~~~~^^^^^^^^^^^^^^^^^^^^^^^^^
    IndexError: list index out of range
    ```

- [__✓__ 40f9231](https://software.franco.net.eu.org/frnmst/md-toc/commit/40f9231)

  Improve benchmark script.

- [__✓__ 1a40e9b](https://software.franco.net.eu.org/frnmst/md-toc/commit/1a40e9b)

  Use Python [dataclass](https://docs.python.org/3.11/library/dataclasses.html)
  in cmark code where appropriate.

- [__✓__ 1a40e9b](https://software.franco.net.eu.org/frnmst/md-toc/commit/1a40e9b)

  Remove debug code in cmark code classes.

### Added

- [#24: *Feature Request: Automatically skip lines to first or (N) TOC*](https://github.com/frnmst/md-toc/issues/24)
- [#37: *Is it possible to ignore certain headers?*](https://github.com/frnmst/md-toc/issues/37)
- [__✓__ 40f9231](https://software.franco.net.eu.org/frnmst/md-toc/commit/40f9231)

  Add use of fuzzer to detect bugs.

### Changed

- [__✓__ c8a6402](https://software.franco.net.eu.org/frnmst/md-toc/commit/c8a6402)

  Change the module imports. To import functions or exceptions from the API
  you now need the full path. For example `md_toc.build_toc` becomes
  `md_toc.api.build_toc`. To do this:
  - the `md_toc/__init__` file needs to have the bare minimum data
  - the API section of the developer interface documentation points to elements
    via their full name
  The purpose of this is to have a cleaner project structure.
- Snake case for all variables.
- Fix and cleanup `md_toc/constants.py`.
- Fix and cleanup `md_toc/types.py`:
  - `class IndentationLogElement`
    - `list marker` -> `list_marker`
    - `indentation_space` -> `indentation_spaces`
  - `class Header`
    - `type` -> `header_type`
  - `class HeaderTypeCounter`
    - `1` ... `6` -> `h1` ... `h6`
  - `class AtxHeadingStructElement`
    - `header type` -> `header_type`
    - `header text trimmed` -> `header_text_trimmed`
- Change default maximum header level to the maximum supported for each parser.
- [__✓__ 40f9231](https://software.franco.net.eu.org/frnmst/md-toc/commit/40f9231)

  Catch `UnicodeDecodeError` exceptions in the
  `md_toc.api.build_toc` function: stop reading the file and print an HTML
  comment.

### Removed

- [__✓__ 88f66bc](https://software.franco.net.eu.org/frnmst/md-toc/commit/88f66bc)

  Remove the deprecated `md_toc.exceptions.CannotTreatUnicodeString` exception.
- [__✓__ 7bbbccc](https://software.franco.net.eu.org/frnmst/md-toc/commit/7bbbccc)

  Remove the `md_toc.api.toc_renders_as_coherent_list` and
  `md_toc.api.init_indentation_status_list` functions and replace them with
  a much simpler check.

## Plan for 8.2.4

### Fixed

- An `md_toc.exceptions.TocDoesNotRenderAsCoherentList` exception is raised for
  big inputs (>= 50M characters) when using the benchmark script.
  There is a jump in the detected header level, 1 then 3, not 2. This always
  happens with the last line of the dummy input file.
- Update existing redcarpet code from version v3.5.0 to version v3.6.0.
  See [issue #28: *Redcarpet v3.5.1 is out*](https://github.com/frnmst/md-toc/issues/28).

### Added

- Add code fence detection before writing TOC in place.
  Until version 8.2.2, given for example a file like this

  ````markdown
  ```

  <!--TOC_MARKER-->

  ```

  # ONE

  ## TWO

  # ONE

  ## TWO

  ### THREE
  ````

  running

  ```shell
  md_toc --toc-marker '<!--TOC_MARKER-->' -p github file.md
  ```

  results in

  ````markdown
  ```

  <!--TOC_MARKER-->

  - [ONE](#one)
    - [TWO](#two)
  - [ONE](#one-1)
    - [TWO](#two-1)
      - [THREE](#three)

  <!--TOC_MARKER-->

  ```

  # ONE

  ## TWO

  # ONE

  ## TWO

  ### THREE
  ````

  instead of

  ````markdown
  ```

  <!--TOC_MARKER-->

  ```

  # ONE

  ## TWO

  # ONE

  ## TWO

  ### THREE
  ````

  This means that if a TOC marker is detected within fenced code block it must
  be ignored. A similar problem happens if there are two TOC markers, one of
  them being inside a fenced code block:

  ````markdown
  ```

  <!--TOC_MARKER-->

  ```

  <!--TOC_MARKER-->

  # ONE

  ## TWO

  # ONE

  ## TWO

  ### THREE
  ````

  using the previous command results in

  ````markdown
  ```

  <!--TOC_MARKER-->

  - [ONE](#one)
    - [TWO](#two)
  - [ONE](#one-1)
    - [TWO](#two-1)
      - [THREE](#three)

  <!--TOC_MARKER-->
  <!--TOC_MARKER-->

  ```

  <!--TOC_MARKER-->

  # ONE

  ## TWO

  # ONE

  ## TWO

  ### THREE
  ````

## Plan for 8.2.3

### Added

- [__✓__ fbf9359](https://software.franco.net.eu.org/frnmst/md-toc/commit/fbf9359)

  Add `./SECURITY.md` file.
- [__✓__ fbf9359](https://software.franco.net.eu.org/frnmst/md-toc/commit/fbf9359)

  Add [pyupgrade](https://github.com/asottile/pyupgrade) to pre-commit.
- [__✓__ 696d334](https://software.franco.net.eu.org/frnmst/md-toc/commit/696d334)

  Add more project metadata and funding info.

- [__✓__ 9edbafc](https://software.franco.net.eu.org/frnmst/md-toc/commit/9edbafc)

  Cleanup pre-commit hooks: remove unused ones and add some from the default
  pre-commit repository.

- [__✓__ 7c6fb22](https://software.franco.net.eu.org/frnmst/md-toc/commit/7c6fb22)

  Add more classifiers in `./setup.cfg`.

### Changed

- [__✓__ 2bf06bb](https://software.franco.net.eu.org/frnmst/md-toc/commit/2bf06bb)

  Remove the `md_toc.generic._utf8_array_to_string` function. Replace
  this with a single Python line in the
  `md_toc.cmark.buffer_c._cmark_cmark_strbuf_put`
  function:

  ```python
  dt = bytearray(data).decode('UTF-8')
  ```

### Fixed

- [__✓__ 2bf06bb](https://software.franco.net.eu.org/frnmst/md-toc/commit/2bf06bb)

  Replace variable correctly in the
  `md_toc.cmark.buffer_c._cmark_cmark_strbuf_put` function.
- [__✓__ fbf9359](https://software.franco.net.eu.org/frnmst/md-toc/commit/fbf9359)

  Fix `/.pre-commit-config.yaml` file indentations.
- [__✓__ fbf9359](https://software.franco.net.eu.org/frnmst/md-toc/commit/fbf9359)

  Cleanup *Pre-commit hook* documentation page.

- [__✓__ 9edbafc](https://software.franco.net.eu.org/frnmst/md-toc/commit/9edbafc)

  Fix some cmark code translated from C: `memmove` functions should now be
  implemented correctly.

- [__✓__ 7c6fb22](https://software.franco.net.eu.org/frnmst/md-toc/commit/7c6fb22)

  Move [Bandit](https://github.com/PyCQA/bandit) configuration from
  `/.pre-commit-config.yaml` to `./pyproject.toml`.

- [__✓__ 7c6fb22](https://software.franco.net.eu.org/frnmst/md-toc/commit/7c6fb22)

  Disable upper pinning Python version, as suggested by
  [this](https://iscinumpy.dev/post/bound-version-constraints/#pinning-the-python-version-is-special)
  document.

### Removed

- [__✓__ 9edbafc](https://software.franco.net.eu.org/frnmst/md-toc/commit/9edbafc)

  Remove an `import` case for Python < 3.8: recent versions of md-toc support
  Python >= 3.8.
