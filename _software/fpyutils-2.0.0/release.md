---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpyutils
software_name_python_module: fpyutils
software_version: 2.0.0
software_version_raw: 000002.000000.000000
release_timestamp: 2021-04-26
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Updated tests.
- Updated package dependencies.

### Removed

- Removal of these functions:
  - `fpyutils.yaml.load_configuration`
- Removal of these modules:
  - `yaml.py`
