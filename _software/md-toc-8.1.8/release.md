---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.1.8
software_version_raw: 000008.000001.000008
release_timestamp: 2023-01-06 18:09:52
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Improved TOC list detection: allow up to 3 leading spaces instead of 0
  for first line of lists inside TOC markers (`<!--TOC-->`) to be detected
  as real TOC content that needs to be replaced.
- Use of a `requirements-freeze.txt` file with pinned dependencies for the
  development environment, computed from the `requirements.txt` and
  `requirements-dev` files.
- Pinned [fpyutils](https://blog.franco.net.eu.org/software/#fpyutils)
  dependency at least to version 3.0.1.
- Use of modern tools to install the package from the PKGBUILD file.
  See
  [Standards based (PEP 517)](https://wiki.archlinux.org/title/Python_package_guidelines#Standards_based_(PEP_517))
  vs
  [setuptools or distutils](https://wiki.archlinux.org/title/Python_package_guidelines#setuptools_or_distutils)

### Removed

- Removed use of `pkg_resources` because they are deprecated.

### Fixed

- Improvements for [issue #38](https://github.com/frnmst/md-toc/issues/38).
- Continue fixing
  [issue #36](https://github.com/frnmst/md-toc/issues/36)
  and split feature table by subject.

Note: GitHub Flavored Markdown is still at version 0.29. md-toc does its best
to support both GFM 0.29 and cmark 0.30.
