---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpyutils
software_name_python_module: fpyutils
software_version: 2.2.0
software_version_raw: 000002.000002.000000
release_timestamp: 2022-06-09 16:40:10
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Added

- Added the `keep_all_lines` argument for the `filelines.get_line_matches` function.

### Changed

- The `fpyutils.filelines.get_line_matches` function now returns the matched lines or the whole line
  depending on the `keep_all_lines` argument.
- Readme is now in markdown.

### Fixed

- Added type annotations.
