---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpyutils
software_name_python_module: fpyutils
software_version: 3.0.1
software_version_raw: 000003.000000.000001
release_timestamp: 2023-01-05 16:25:06
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Use of a `requirements-freeze.txt` file with pinned dependencies for the
  development environment, computed from the `requirements.txt` and
  `requirements-dev` files.
- Use of modern tools to install the package from the PKGBUILD file.
  See
  [Standards based (PEP 517)](https://wiki.archlinux.org/title/Python_package_guidelines#Standards_based_(PEP_517))
  vs
  [setuptools or distutils](https://wiki.archlinux.org/title/Python_package_guidelines#setuptools_or_distutils)
- Fixed and updated badges in `./README.md`.

### Removed

- Removed use of `pkg_resources` because they are deprecated.

### Fixed

- Improved file atomic writing: `OSError: [Errno 18] Invalid cross-device link`
  came up in Fedora, Arch Linux and WSL. Changing `os.replace` to `shutil.move`
  seems to fix the problem in Fedora and Arch Linux.
  See also [md-toc issue #38](https://github.com/frnmst/md-toc/issues/38).
