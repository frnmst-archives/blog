---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: docker-debian-postgis-django
software_name_python_module: docker_debian_postgis_django
software_version: 2.0.2
software_version_raw: 000002.000000.000002
release_timestamp: 2021-03-10 15:10:01
is_on_pypi: false
has_changelog: false
signing_public_key: pgp_pubkey_since_2019.txt
---
