---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fattura-elettronica-reader
software_name_python_module: fattura_elettronica_reader
software_version: 2.0.7
software_version_raw: 000002.000000.000007
release_timestamp: 2021-07-31 20:43:45
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Updated file checksum variable.
- Updated readme.
- Added git hooks.
- Updated package dependencies.
