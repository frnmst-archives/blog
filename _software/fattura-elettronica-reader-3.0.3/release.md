---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fattura-elettronica-reader
software_name_python_module: fattura_elettronica_reader
software_version: 3.0.3
software_version_raw: 000003.000000.000003
release_timestamp: 2022-06-24 13:55:07
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Updated file checksum.
- Updated dependencies.
- Updated hooks.

### Fixed

- Fixed gitignore file.
