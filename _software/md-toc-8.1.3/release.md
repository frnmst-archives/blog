---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.1.3
software_version_raw: 000008.000001.000003
release_timestamp: 2022-04-20 10:22:31
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---


### Deprecated

- Deprecation of `md_toc.exceptions.CannotTreatUnicodeString` exception.
  This exception will be removed in version 9 of md-toc.

### Fixed

- Memory improvements for operations on class instances.
- Speed improvements for concatenation operations on strings.
- Improved constants file.
- Added square bracket support detection in emphasis for cmark.
  See [issue 25](https://github.com/frnmst/md-toc/issues/25)

Note: GitHub Flavored Markdown is still at version 0.29. md-toc does its best to
      support both GFM 0.29 and cmark 0.30.
