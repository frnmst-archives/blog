---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpydocs
software_name_python_module: fpydocs
software_version: 4.1.1
software_version_raw: 000004.000001.000001
release_timestamp: 2021-09-29 09:20:31
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Improved workflow common documentation.
- Updated package dependencies.

### Added

- Added option in the `Makefile` blueprint.
