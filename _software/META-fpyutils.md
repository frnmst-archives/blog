---
meta: true
software_name: fpyutils
archived: false
canonical_repository_url: https://software.franco.net.eu.org/frnmst/fpyutils
codeberg_repository_url: https://codeberg.org/frnmst/fpyutils
framagit_repository_url: https://framagit.org/frnmst/fpyutils
github_repository_url: https://github.com/frnmst/fpyutils
documentation_url: https://docs.franco.net.eu.org/fpyutils
documentation_ref: docs.franco.net.eu.org/fpyutils
has_changelog: true
related_links:
  - label: 'Repology python:fpyutils (package tracking)'
    url: 'https://repology.org/project/python:fpyutils/versions'
  - label: 'Repology fpyutils (package tracking)'
    url: 'https://repology.org/project/fpyutils/versions'
  - label: 'Libraries.io'
    url: 'https://libraries.io/pypi/fpyutils'
  - label: 'Liberapay'
    url: 'https://en.liberapay.com/frnmst/'
  - label: 'Buy Me a Coffee'
    url: 'https://www.buymeacoffee.com/frnmst'
---
