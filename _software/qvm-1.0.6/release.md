---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: qvm
software_version: 1.0.6
software_version_raw: 000001.000000.000006
release_timestamp: 2021-04-23 22:25:05
is_on_pypi: false
has_changelog: false
signing_public_key: pgp_pubkey_since_2019.txt
---
