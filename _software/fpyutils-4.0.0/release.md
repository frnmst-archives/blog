---
layout: software_release
enable_markdown: true
title: release
excerpt: none
tags: [fpyutils, utilities, python]
software_name: fpyutils
software_name_python_module: fpyutils
software_version: 4.0.0
software_version_raw: 000004.000000.000000
release_timestamp: 2023-11-06 18:59:12
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Change the module imports. To import functions or exceptions from the API
  you now need the full path. For example `fpyutils.insert_string_at_line`
  becomes `fpyutils.filelines.insert_string_at_line`. To do this:
  - the `fpyutils/__init__` file has the bare minimum data
  - the API section of the developer interface documentation points to elements
    via their full name
  The purpose of this is to have a cleaner project structure.

  __*note:*__ __This might break the imports on existing programs that use fpyutils
  as dependency. Update accordingly!__

- Update Sphinx and its theme to their latest possible versions
- Update software dependencies
- Minimum supported Python version is now `3.8`.

### Fixed

- Use `f.readline()` instead of the single

  ```python
  with open(input_file, 'r') as f:
        lines: list = f.readlines()
  ```

  operation in the `fpyutils.filelines.insert_string_at_line` function.
  This should decrease memory usage.
- Improve typing annotations
