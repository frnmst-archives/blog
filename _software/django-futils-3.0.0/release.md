---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: django-futils
software_name_python_module: django_futils
software_version: 3.0.0
software_version_raw: 000003.000000.000000
release_timestamp: 2021-07-06 17:52:34
is_on_pypi: false
has_changelog: false
signing_public_key: pgp_pubkey_since_2019.txt
---
