---
meta: true
software_name: licheck
archived: false
canonical_repository_url: https://software.franco.net.eu.org/frnmst/licheck
codeberg_repository_url: https://codeberg.org/frnmst/licheck
framagit_repository_url: https://framagit.org/frnmst/licheck
github_repository_url:
documentation_url: https://docs.franco.net.eu.org/licheck
documentation_ref: docs.franco.net.eu.org/licheck
has_changelog: true
---
