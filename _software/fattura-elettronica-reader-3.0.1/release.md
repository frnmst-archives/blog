---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fattura-elettronica-reader
software_name_python_module: fattura_elettronica_reader
software_version: 3.0.1
software_version_raw: 000003.000000.000001
release_timestamp: 2022-02-21 16:08:33
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Updated dependencies.
- Updated documentation URLs.

### Fixed

- Added a missing underscore of a variable name in the `fattura_elettronica_reader.api.pipeline` function.
