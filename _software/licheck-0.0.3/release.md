---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: licheck
software_name_python_module: licheck
software_version: 0.0.3
software_version_raw: 000000.000000.000003
release_timestamp: 2021-08-10 20:59:34
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Fixed

- Cache from different repositories is now preserved.
