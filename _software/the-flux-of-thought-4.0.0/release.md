---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: the-flux-of-thought
software_version: 4.0.0
software_version_raw: 000004.000000.000000
release_timestamp: 2021-04-22 17:55:07
is_on_pypi: false
has_changelog: false
signing_public_key: pgp_pubkey_since_2019.txt
---
