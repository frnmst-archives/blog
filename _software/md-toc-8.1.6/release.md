---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.1.6
software_version_raw: 000008.000001.000006
release_timestamp: 2022-12-08 17:23:18
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Fixed

- Until version 8.1.5, given for example a file like this

  ```markdown
  # ONE

  ## TWO

  ### THREE

  ### THREE

  ### THREE

  ## THREE

  # ONE
  ```

  running

  ```shell
  md_toc github -l2 file.md
  ```

  results in

  ```markdown
  - [ONE](#one)
    - [TWO](#two)
    - [THREE](#three)
  - [ONE](#one-1)
  ```

  instead of

  ```markdown
  - [ONE](#one)
    - [TWO](#two)
    - [THREE](#three-3)
  - [ONE](#one-1)
  ```

  Note the `-l2` option.

  md-toc works fine if you use the maximum header level (typically 6).
  Add a `visible` key in the returned `struct` of the
  `md_toc.api.get_atx_heading` function so that correct duplicate header counts
  can be performed. This means that in cases of non
  maximum header level selection (typically < 6) using the
  `-l [{1,2,3,4,5,6}], --header-levels [{1,2,3,4,5,6}]` option, all headers
  must be parsed even if they are not visible in the end.
- Update documentation.
- Change Sphinx theme.

Note: GitHub Flavored Markdown is still at version 0.29. md-toc does its best to support both GFM 0.29 and cmark 0.30.
