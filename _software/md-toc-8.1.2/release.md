---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: md-toc
software_name_python_module: md_toc
software_version: 8.1.2
software_version_raw: 000008.000001.000002
release_timestamp: 2022-04-03 14:29:18
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Fixed

- Added code span support detection in emphasis for cmark.
  See [issue 25](https://github.com/frnmst/md-toc/issues/25)
- Ported missing code changes from cmark 0.29 to 0.30.

  Note: GitHub Flavored Markdown is still at version 0.29. md-toc does its best to
        support both GFM 0.29 and cmark 0.30.

### Changed

- All cmark code has been split in separate Python modules:
  each module represents a C source file.
- Documentation about markdown specification has been split in various files
  to improve readability.
- Updated copyright headers.
