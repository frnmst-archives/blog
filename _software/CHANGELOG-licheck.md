---
layout: changelog
enable_markdown: true
software_name: licheck
title: licheck changelog
excerpt: none
---

## Unreleased

- Support for packages with multiple licenses when new version of *dep-license* is
  released.
