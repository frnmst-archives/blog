---
meta: true
software_name: qvm
archived: true
canonical_repository_url: https://software.franco.net.eu.org/frnmst-archives/qvm
codeberg_repository_url: https://codeberg.org/frnmst-archives/qvm
framagit_repository_url: https://framagit.org/frnmst-archives/qvm
github_repository_url:
documentation_url: https://software.franco.net.eu.org/frnmst-archives/qvm#qvm
documentation_ref: software.franco.net.eu.org/frnmst-archives/qvm#qvm
has_changelog: false
---
