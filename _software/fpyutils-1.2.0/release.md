---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpyutils
software_name_python_module: fpyutils
software_version: 1.2.0
software_version_raw: 000001.000002.000000
release_timestamp:
is_on_pypi: false
has_changelog: false
signing_public_key: pgp_pubkey_since_2019.txt
---
