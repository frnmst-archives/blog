---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: automated-tasks
software_name_python_module: automated_tasks
software_version: 13.0.0
software_version_raw: 000013.000000.000000
release_timestamp: 2021-12-31 16:31:43
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Added attachment extraction for `archive_invoice_files.py`. This adds new required
  variables in the configuration file.
- Use of hardcoded sub directory called `submodules` for `build_python_packages.py`.
  This is useful to avoid having hundreds of dicrectory in the repository root.
- Removed `--recursive` option when downloading repository as submodules in `build_python_packages.py`.
  This avoids connecting to external resources (usually github.com). The
  `--recursive` option might still be needed to build certain packages!
- Updated to PyYAML 6
- Added checksum files in `save_and_notify_file_diffs.py`. These files are text files
  containing SHA512 checksums for each exising file in the repository.
- Added use of RSS (XML) feed files for `command_assert.py`. An RSS file will
  be generated for each configuration file. Generation is optional but the variables
  must be present.

### Added

- Added new scripts:
  - `firefox_profile_runner.py`
  - `push_files.py`
