---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: licheck
software_name_python_module: licheck
software_version: 1.0.0
software_version_raw: 000001.000000.000000
release_timestamp: 2022-01-12 18:33:10
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Added

- Added these functions:
  - `licheck.api.check_dependencies_files_data_structure`
  - `licheck.api.create_cache_output`
  - `licheck.api.is_sha512_checksum`
  - `licheck.api.save_cache`
- Added these exceptions:
  - `licheck.exceptions.IncoherentData`
  - `licheck.exceptions.NotAChecksum`

### Changed

- Cache is now written with a single operation instead of
  multiple appends.
- API change for the `licheck.api.write_cache` function.
