---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: automated-tasks
software_name_python_module: automated_tasks
software_version: 11.3.0
software_version_raw: 000011.000003.000000
release_timestamp: 2021-10-31 22:09:14
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Use of more relaxed version of the Python dependencies.

### Fixed

- Fixed `iptables-geoport.service` unit file so that it saves iptables
  rules persistently.
- Improved documentation for the `qvm.py` script.

### Added

- Added `build_python_packages.py` script.
- Added new makefile instructions.
