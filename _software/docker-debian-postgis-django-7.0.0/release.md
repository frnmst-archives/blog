---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: docker-debian-postgis-django
software_name_python_module: docker_debian_postgis_django
software_version: 7.0.0
software_version_raw: 000007.000000.000000
release_timestamp: 2021-12-09 16:20:36
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Fixed

- Updated copyright headers.

### Changed

- Updated Dockerfile dependencies.
- Updated Dockerfile base image version.

### Added

- New PyPI mirror argument for:
  - `Makefile.dist`
  - `Dockerfile.dist`
