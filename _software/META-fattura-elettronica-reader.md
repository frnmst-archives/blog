---
meta: true
software_name: fattura-elettronica-reader
archived: false
canonical_repository_url: https://software.franco.net.eu.org/frnmst/fattura-elettronica-reader
codeberg_repository_url: https://codeberg.org/frnmst/fattura-elettronica-reader
framagit_repository_url: https://framagit.org/frnmst/fattura-elettronica-reader
github_repository_url:
documentation_url: https://docs.franco.net.eu.org/fattura-elettronica-reader
documentation_ref: docs.franco.net.eu.org/fattura-elettronica-reader
has_changelog: true
---
