---
layout: changelog
enable_markdown: true
software_name: fattura-elettronica-reader
title: fattura-elettronica-reader changelog
excerpt: none
---

## Plan for 3.0.6

### Fixed

- Use of list comprehensions instead of for loops to possibly improve speed.

### Changed

- Use [platformdirs](https://github.com/platformdirs/platformdirs)
  instead of [appdirs](https://github.com/ActiveState/appdirs).
