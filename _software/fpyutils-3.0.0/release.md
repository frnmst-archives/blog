---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpyutils
software_name_python_module: fpyutils
software_version: 3.0.0
software_version_raw: 000003.000000.000000
release_timestamp: 2022-12-28 18:09:49
is_on_pypi: true
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Removed

- Removed [python-atomicwrites](https://github.com/untitaker/python-atomicwrites)
  (`atomicwrites` is the Python requirement name) library dependency because
  of its deprecation in July 2022. Atomic writes are now done through
  temporary files without external libraries, as
  [suggested](https://github.com/untitaker/python-atomicwrites/blob/4183999d9b7e81af85dee070d5311299bdf5164c/README.rst#unmaintained)
  by the author of python-atomicwrites.
- Removed [requests](https://github.com/psf/requests) library dependency and
  replaced the only use of it in fpyutils with the build-in
  `urllib.request` module.

### Fixed

- Use of list comprehensions instead of for loops to possibly improve speed.
- Pinned YAPF and flake8 versions through the pre-commit update command
  in the Makefile.
- Fixed pre-commit hooks.
- Improved fpyutils.filelines functions: instead of multiple write operations
  in for loops do a single write operation before the end of the functions.
- Use of `f.readline()` instead of `f.readlines()` in the
  `fpyutils.filelines.remove_line_interval` function. This should decrease
  memory usage.
- Fixed `MANIFEST.in` and `check-manifest` pre-commit hook configuration.

### Changed

- The `fpyutils.notify.send_gotify_message` now returns a
  `http.client.HTTPResponse` object instead of a `requests.Response` object.
- Implemented use of `setup.cfg` and `pyproject.toml`.
  `setup.py` is now a dummy file.
- Moved some pre-commit options to `setup.cfg`.
- Changed Sphinx theme.
- Replaced Pipenv environment with `venv` and `pip` commands.
- Replaced distribution commands (`setup.py sdist`, `setup.py bdist_wheel`)
  with the [`build`](https://pypa-build.readthedocs.io/en/stable/) module.
