---
meta: true
software_name: docker-debian-postgis-django
archived: false
canonical_repository_url: https://software.franco.net.eu.org/frnmst/docker-debian-postgis-django
codeberg_repository_url: https://codeberg.org/frnmst/docker-debian-postgis-django
framagit_repository_url: https://framagit.org/frnmst/docker-debian-postgis-django
github_repository_url:
documentation_url: https://software.franco.net.eu.org/frnmst/docker-debian-postgis-django#docker-debian-postgis-django
documentation_ref: software.franco.net.eu.org/frnmst/docker-debian-postgis-django#docker-debian-postgis-django
has_changelog: true
---
