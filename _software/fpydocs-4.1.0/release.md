---
layout: software_release
enable_markdown: true
title: release
excerpt: none
software_name: fpydocs
software_name_python_module: fpydocs
software_version: 4.1.0
software_version_raw: 000004.000001.000000
release_timestamp: 2021-08-23 17:11:39
is_on_pypi: false
has_changelog: true
signing_public_key: pgp_pubkey_since_2019.txt
---

### Changed

- Improved existing blueprints.

### Added

- Added and updated some pre-commit hooks.
