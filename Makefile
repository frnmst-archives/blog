# Copyright (c) 2017 Franco Masotti.
# See LICENSE file for details.

PORT = 3050
# See
# https://docs.python.org/3/library/venv.html#how-venvs-work
export VENV_CMD=. .venv/bin/activate

default: install-dev

all: clean build serve-global

install-dev:
	python3 -m venv .venv
	$(VENV_CMD) \
		&& pip install --requirement requirements-freeze.txt \
		&& deactivate
	$(VENV_CMD) \
		&& pre-commit install \
		&& deactivate
	$(VENV_CMD) \
		&& pre-commit install --hook-type commit-msg \
		&& deactivate
	bundle install

regenerate-freeze: uninstall-dev
	python3 -m venv .venv
	$(VENV_CMD) \
		&& pip install --requirement requirements.txt --requirement requirements-dev.txt \
		&& pip freeze --local > requirements-freeze.txt \
		&& deactivate

uninstall-dev:
	rm -rf .venv

update: install-dev
	 $(VENV_CMD) \
		 && pre-commit autoupdate \
	 	 && deactivate

pre-commit:
	$(VENV_CMD) \
		&& pre-commit run --all \
		&& deactivate

build:
	bundle exec jekyll build --config=_config.yml,_config_dev.yml --trace --strict_front_matter --verbose --safe -t --future

serve:
	bundle exec jekyll serve --config=_config.yml,_config_dev.yml --trace --future

serve-global:
	bundle exec jekyll serve --config=_config.yml,_config_dev.yml --trace --host=0.0.0.0 --port=$(PORT) --future

clean:
	rm -rf _site

.PHONY: default install-dev regenerate-freeze uninstall-dev all build serve serve-global clean
