---
title: Disable IPv6 on Linux and OpenWRT
tags: [ipv6, gnu-linux, openwrt, video, youtube]
related_links: ['https://youtu.be/UpmMzQLmR18']
updated: 2023-07-29 22:15:00
description: Disabling IPv6 on GNU/Linux and OpenWRT is easy and sometimes there are valid reasons to do it
canonical_url: 'https://youtu.be/UpmMzQLmR18'
---
Disable IPv6 on Linux and OpenWRT

Let's see how and why disabling IPv6 on GNU/Linux systems and on OpenWRT routers.

Warning: disabling IPv6 may have unintended consequences. Services are designed to run in dual stack mode. If you want to run a server exclusively in IPv4 mode you'll need to change lots of configuration files, potentially one for each service!

<iframe width="560" height="315" src="https://www.youtube.com/embed/UpmMzQLmR18" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
