---
title: ! 'A. I. text generation in Nextcloud'
tags: [nextcloud, ai, localai, chatgpt, shorts, video, youtube]
updated: 2023-10-30 18:00:00
description: ! 'Using the smart picker menu in Nextcloud to generate text like you would with ChatGPT'
canonical_url: 'https://youtube.com/shorts/GDIlZcFEH2A'
---

In this YouTube short I show the ChatGPT-like text generation which is
available since Nextcloud 26 through the *smart picker* menus.

Just like Nextcloud, the A.I. models and server are self-hosted using
[LocalAI](https://localai.io/). The problem with this approach is that using a
CPU to generate text or pictures is very very slow: expect between 2 to 3
minutes for each response. The Nextcloud frontend is not happy about this,
in-fact its default timeout is only 6 seconds. This messes up the LocalAI
Docker container which needs to be restarted. Another problem is that not all
models are supported by standard CPUs.

So, if you have the right hardware and find the right A. I. models maybe you
can get something useful from these new tools.

This remains only an experiment for me at the moment.

<iframe width="483" height="859" src="https://www.youtube.com/embed/GDIlZcFEH2A" title="A. I. text generation in Nextcloud #Shorts" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
