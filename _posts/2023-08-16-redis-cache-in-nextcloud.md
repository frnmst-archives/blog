---
title: Redis cache in Nextcloud
tags: [nextcloud, redis, cache, socket, video, youtube]
updated: 2023-08-16 17:00:00
description: ! 'Setting up Redis to be used as cache for Nextcloud: Nextcloud is served by a Docker container while Redis is in the host. Redis is accessible through a socket instead of the default TCP/IP port'
canonical_url: 'https://youtu.be/MRr1cl3Ijgo'
---
In this video I show you how to setup Redis as cache for Nextcloud. Using an in-memory cache such as Redis improves overall performance of your Nextcloud instance.

This particular way of setting up Redis is not typical: Redis is configured to be accessible through a socket instead of the default TCP/IP port. This socket is then shared through a Docker volume.

<iframe width="560" height="315" src="https://www.youtube.com/embed/MRr1cl3Ijgo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
