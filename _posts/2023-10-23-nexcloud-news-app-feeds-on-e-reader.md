---
title: ! 'Nexcloud News app feeds on e-reader'
tags: [nextcloud, ebook, rss, tutorial, video, youtube]
updated: 2023-10-23 17:00:00
description: ! 'Read RSS feeds offline on an e-reader from Nextcloud news sources'
canonical_url: https://youtu.be/e16eZPQm9M4
---

If you have a Nextcloud instance you probably use the
[News app](https://apps.nextcloud.com/apps/news). This software offers an
[API](https://nextcloud.github.io/news/api/api-v1-3/), so you can interact
with the app.

In this video you'll see a script I written to download the feeds and read them
offline on a e-reader. The reason for this is to have something similar to
a *newspaper experience*, if your reader uses the *e-ink* technology or
something comparable.

<iframe width="560" height="315" src="https://www.youtube.com/embed/e16eZPQm9M4?si=2s59umtm0Mr-Kirz" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
