---
title: A simple way to fix audio on GNU/Linux
tags: [audio, alsa, pulseaudio, linux, webcam, hdmi, tutorial]
updated: 2022-10-23 17:40:00
description: Passing from ALSA to Pulseaudio to handle payback and capture devices seamessly
---

I wanted to set HDMI as default output and the webcam's microphone as default
input. Using ALSA directly didn't work in all cases so I tried with Pulseaudio.

## First attempt: use ALSA

I did several tests to see if it was feasable with ALSA. Turns out it isn't
so straight forward.

### Output

1. get a list of all output devices

   ```shell
   aplay -l
   ```

   which in my case results in

   ```
   **** List of PLAYBACK Hardware Devices ****
   card 0: PCH [HDA Intel PCH], device 0: ALC887-VD Analog [ALC887-VD Analog]
     Subdevices: 1/1
     Subdevice #0: subdevice #0
   card 0: PCH [HDA Intel PCH], device 1: ALC887-VD Digital [ALC887-VD Digital]
     Subdevices: 1/1
     Subdevice #0: subdevice #0
   card 0: PCH [HDA Intel PCH], device 3: HDMI 0 [HDMI 0]
     Subdevices: 1/1
     Subdevice #0: subdevice #0
   card 0: PCH [HDA Intel PCH], device 7: HDMI 1 [HDMI 1]
     Subdevices: 1/1
     Subdevice #0: subdevice #0
   card 0: PCH [HDA Intel PCH], device 8: HDMI 2 [HDMI 2]
     Subdevices: 0/1
     Subdevice #0: subdevice #0
   card 0: PCH [HDA Intel PCH], device 9: HDMI 3 [HDMI 3]
     Subdevices: 1/1
     Subdevice #0: subdevice #0
   card 0: PCH [HDA Intel PCH], device 10: HDMI 4 [HDMI 4]
     Subdevices: 1/1
     Subdevice #0: subdevice #0
   ```

2. this configuration sets Intel's HDMI audio as default device. I had to go through all the
   HDMI Intel devices to know which one to choose.
   See this
   [Arch wiki page](https://wiki.archlinux.org/title/Advanced_Linux_Sound_Architecture/Troubleshooting#HDMI_Output_does_not_work).
   Add this to `~/.asoundrc`

   ```configuration
   pcm.!default {
       type hw
       card 0
       device 8
   }
   defaults.pcm.card 0
   defaults.pcm.device 8
   defaults.ctl.card 0
   ```

### Input

1. Now, list all the input devices

   ```shell
   arecord -l
   ```

   which outputs

   ```
   **** List of CAPTURE Hardware Devices ****
   card 0: PCH [HDA Intel PCH], device 0: ALC887-VD Analog [ALC887-VD Analog]
     Subdevices: 1/1
     Subdevice #0: subdevice #0
   card 0: PCH [HDA Intel PCH], device 2: ALC887-VD Alt Analog [ALC887-VD Alt Analog]
     Subdevices: 1/1
     Subdevice #0: subdevice #0
   card 1: STK1160 [STK1160], device 0: USB Audio [USB Audio]
     Subdevices: 1/1
     Subdevice #0: subdevice #0
   card 2: HD3000 [Microsoft® LifeCam HD-3000], device 0: USB Audio [USB Audio]
     Subdevices: 1/1
     Subdevice #0: subdevice #0
   ```

2. this configuration is used to set the webcam's audio as default capture device.
   Add this to `~/.asoundrc` (this does not work in conjunction with the previous
   ALSA configuration)

   ```configuration
   pcm.usb
   {
       type hw
       card HD3000
   }

   pcm.!default
   {
       type asym
       playback.pcm
       {
           type plug
           slave.pcm "usb"
       }
       capture.pcm
       {
           type plug
           slave.pcm "usb"
       }
   }
   ```

Some applications such as Firefox will not see these configurations and will
continue to use the system's default audio devices. Good luck accessing
a non default microphone or playing back on HDMI on a browser.
[This page](https://alsa.opensrc.org/MultipleCards) explains how to deal with
multiple audio cards as well as ordering them in the kernel.

## Second attempt: use Pulseaudio

Pulseaudio is used a layer on top of ALSA. There is no need to setup devices
manually with configuration files and applications have less difficuly
accessing the audio.

1. install Pulseaudio and enable the daemon

   ```shell
   apt-get install pulseaudio pavucontrol pamix
   systemctl --user enable pulseaudio.service
   ```

2. get the names and kill the applications still accessing ALSA. Run this as root

   ```shell
   fuser -v /dev/snd/*
   ```

3. backup your ALSA configuration, just in case

   ```shell
   mv ~/.asoundrc ~/.asoundrc.bak
   ```

4. start the service

   ```shell
   systemctl --user start pulseaudio.service
   ```

5. open the mixers and enjoy

   ```shell
   pavucontrol
   pamix
   ```

## Improvements

- [Enable High Quality Audio on Linux \| by Gamunu Balagalla \| Medium](https://medium.com/@gamunu/enable-high-quality-audio-on-linux-6f16f3fe7e1f)
