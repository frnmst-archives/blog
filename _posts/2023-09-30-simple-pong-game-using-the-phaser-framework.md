---
title: ! 'Simple PONG game using the Phaser framework'
tags: [phaser-3, pong, game, javascript, video, youtube]
updated: 2023-09-30 21:45:00
description: ! 'This video shows a PONG game written in Javascript using the Phaser 3 framework'
canonical_url: https://youtu.be/1FsPx4jJBM0
---

Creating a simple PONG game using the Phaser 3 framework in JavaScript.

[Try the game](https://assets.franco.net.eu.org/web/pong/pong.html)

<iframe width="560" height="315" src="https://www.youtube.com/embed/1FsPx4jJBM0?si=ArIlHLnMUJGqCPgO" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
