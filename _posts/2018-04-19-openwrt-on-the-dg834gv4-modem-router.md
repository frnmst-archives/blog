---
title: OpenWrt on the DG834Gv4 modem router
tags: [openwrt, modem, router, backdoor, ssh]
updated: 2023-02-15 17:08:00
description: 'Installing OpenWrt on the DG834Gv4 modem router'
images:
  - id: 'dg834g'
    filename: 'dg834gv4_leds.gif'
    alt: 'DG834Gv4 leds'
    caption: ! 'DG834Gv4 with leds working'
---

# Overview

{% include image.html id="dg834g" %}

<!--excerpt_start-->
The [DG834Gv4](https://wikidevi.com/wiki/Netgear_DG834Gv4) is an old
*Netgear* WiFi modem/router and has been my first wireless access<!--excerpt_end-->
point. Years have passed and this hardware shows signs
of its age. It is compliant to
[802.11 b/g](https://en.wikipedia.org/wiki/IEEE_802.11#802.11g) only and
has 100 Mbps ethernet ports .
Also, when using it as a DSL modem, it sometimes disconnects
because of the heat it generates. It was time to decommission it.

In the meantime someone discovered a *nice*
[backdoor](https://github.com/elvanderb/TCP-32764)
[feature](https://wikidevi.com/wiki/TCP-32764) in a series of
modem/routers, including the DG834Gv4. One more reason not to use it *as-is*.

*After the router collected some dust*, I evaluated some options such as
installing OpenWrt or the [DGTeam](http://www.dgteam-mirror.talktalk.net/Home.html) firmwares.
I went with the DGTeam firmware at first because it had modem support. This
meant I could use it as a spare modem.

A few weeks ago, however, I tried installing OpenWrt and it wasn't so straight
forward as I thought.

# Steps

- [Follow the instuctions reported on the wiki](https://openwrt.org/toh/netgear/dg834g.v4?s[]=dg834gv4)
  but instead of getting Chaos Calamer get
  [Attitude Adjustment](https://archive.openwrt.org/attitude_adjustment/12.09/brcm63xx/generic/openwrt-96348GW-10-generic-squashfs-cfe.bin).
  Use `nftp.c` to flash the image on it. This will work even of you brick the
  router.

  The reason to select such an old image is because of the insufficient free
  RAM available once the important services start up. Even with Attitude
  Adjustment you'll run out of memory easily. I also tried using older versions
  of OpenWrt without success.

- Go to [http://192.168.1.1](http://192.168.1.1) and change the root password via the *LuCI* web interface.
  This will also enable the SSH daemon.

- Login via SSH (`ssh root@192.168.1.1`) and run the following to remove
  the web interface.

  ```shell
  /etc/init.d/uhttpd stop
  /etc/init.d/uhttpd disable
  # repeat the following until no more packages are removed.
  opkg remove --autoremove luci*
  ```

## Network and device configuration

- Time configure the network interfaces with `vi`:

  ```shell
  root@OpenWrt:~# cat /etc/config/wireless
  config wifi-device  radio0
      option type     'mac80211'
      option macaddr  '<router_s MAC address here>'
      option hwmode   '11g'
      option channel '<channel>'
      option country '<region>'
      option txpower '20'

      # REMOVE THIS LINE TO ENABLE WIFI:
      option disabled '0'

  config wifi-iface
      option device   'radio0'
      option network  'lan'
      option mode     'ap'
      option encryption    'psk2+aes'
      option key      '<your key here>'
      option ssid     '<your SSID here>'

  root@OpenWrt:~# cat /etc/config/network

  config interface 'loopback'
      option ifname 'lo'
      option proto 'static'
      option ipaddr '127.0.0.1'
      option netmask '255.0.0.0'

  config interface 'lan'
      option ifname 'eth0'
      option type 'bridge'
      option proto 'static'
      option ipaddr '<router_s address>'
      option netmask '<router_s netmask>'
  ```

- If you want to use the router as a switch/repeater, this will disable the
  DHCP server:

  ```shell
  /etc/init.d/dnsmasq stop
  /etc/init.d/dnsmasq disable
  ```

# Problems

There is not enough memory to install anything even after killing several
processes.

```shell
route add default gateway 192.168.1.1 netmask 0.0.0.0 metric 0
echo "nameserver 8.8.8.8" > /etc/resolv.conf

root@OpenWrt:~# opkg install sshfs
  Installing sshfs (2.2-1) to root...
  Downloading http://downloads.openwrt.org/attitude_adjustment/12.09/brcm63xx/generic/packages/sshfs_2.2-1_brcm63xx.ipk.
  Collected errors:
     * gz_open: fork: Cannot allocate memory.
     * opkg_install_pkg: Failed to unpack control files from /tmp/opkg-Ry3bvT/sshfs_2.2-1_brcm63xx.ipk.
     * opkg_install_cmd: Cannot install package sshfs.
```

For some reason, WiFi seems less powerful (as in range) than it should be.
This is quite disappointing.

# Alternatives

It would be interesting to patch the original Netgear/SerComm firmware by
removing the `scfgmgr` file and a line in a shell script that is launched at boot.

You need a 32 bit GNU/Linux distribution with the appropriate development
libraries and only then you'll be able to run the software that rebuilds the
image. Unfortunately there is only a text file describing generic instructions
so it takes quite some time to track down all the necessary libraries and to have
a working environment. Maybe another time...

# Applications using OpenWrt

I ended up using it for this:
[openwrt-leds-as-notification](https://software.franco.net.eu.org/frnmst-archives/openwrt-leds-as-notification)
