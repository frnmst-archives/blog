---
title: Running python scripts using different package versions
tags: [tutorial, python, python3, pacman, pip, venv, script, shell, bash, aur]
updated: 2022-10-08 15:33:00
description: A couple of ways to prevent file conflicts on python packages and run scripts without package version problems
---

## Introduction

<!--excerpt_start-->
Installing Python packages from pacman as well as pip, using the root user,
is not a good idea because pacman cannot keep track of files<!--excerpt_end-->
written by pip in the installation directories. File conflicts
are easy to come out.

<!--more-->

This problem stands out especially when using Python 3 packages
installed from AUR because of their lack of maintenance. In this case
I was tempted to run ``pip3 install ${PACKAGE}``...

Have a look at [this article](https://opensource.com/article/19/4/managing-python-packages)
which is released under the Creative Commons Attribution-ShareAlike 4.0 International License
by László Kiss Kollár.

## Solutions

Just as the original article explains, I suggest two methods:

1. use the user installation method if you just need the executable
2. use the virtual environment method if you need to import a module
   for a script.

The only problem of these two systems is that package updates need to be
handled manually.

### User installation method

Add the following to your shell's configuration (with GNU Bash use `~/.bashrc`):

    export PATH=$HOME/.local/bin:$PATH

Reload your shell and run:

    pip3 install "${PACKAGE_NAME}" --user

You can now call the executable just like you would do after
installing a package using the root user (`# pip3 install "${PACKAGE_NAME}"`)

### Virtual environment method

Install virtualenv and then create a new virtual environment:

```shell
python3 -m venv ~/.local/venv/"${ENVIRONMENT_NAME}"
. ~/.local/venv/"${ENVIRONMENT_NAME}"/bin/activate
pip3 install ${YOUR_PACKAGES}
deactivate
```

To run a script using the newly created virtual environment
you must call the virtual interpreter directly:

    ~/.local/venv/"${ENVIRONMENT_NAME}"/bin/python3 "${SCRIPT_PATH}"

Finally, add the path of the virtual environment executables to
the shell's path:

    export PATH=$PATH:$HOME/.local/venv/"${ENVIRONMENT_NAME}"/bin

Reload your shell to be able to call a virtual environment executable directly.

## Conclusion

Don't mess up your Python installation and have fun :)
