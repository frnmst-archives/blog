---
title: ! 'Xonotic: playing on the "Vehicles" server'
tags: [xonotic, gaming, linux, video, youtube]
updated: 2023-11-11 18:00:00
description: ! 'Xonotic, playing the FOSS FPS game on the "Vehicles" server'
canonical_url: 'https://youtu.be/cVzgAtRbiLk'
---

Xonotic is an FPS game released under the GPLv3+ license.

In this video I show you a complete unedited online CTF (Capture The Flag)
match on this server which uses vehicles and other mods.

You don't need an expensive GPU to run this: even an older integrated GPU will
work fine. I was able, in-fact, to play and record the video on the same
machine using an integrated Intel graphics GPU.

Hve fun :)

<iframe width="560" height="315" src="https://www.youtube.com/embed/cVzgAtRbiLk?si=jtm747VEvp_iClW4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
