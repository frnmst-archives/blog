---
title: Changing domain of a Gitea instance
tags: [tutorial, domain, gitea, apache, letsencrypt]
updated: 2023-08-07 17:29:00
description: Some steps to change a domain of a Gitea instance.
---

<!--excerpt_start-->
Here I am assuming that you are using Apache as webserver and Let's Encrypt as<!--excerpt_end-->
certificate authority, as well as:

- old domain: old.domain
- new domain: new.domain

Now, follow these instructions:

1. edit the following Apache virtual hosts variables by replacing `old.domain` with `new.domain`:

   ```
   ServerName
   RewriteCond
   ```

   but keep these:

   ```
   SSLCertificateKeyFile
   SSLCertificateFile
   ```

2. `systemctl restart apache2`
3. `certbot --apache`
4. select the domain number when asked this question:

   ```
   Plugins selected: Authenticator apache, Installer apache

   Which names would you like to activate HTTPS for?
   ```

5. redirect should already be in place, so select option 1: no redirect

   ```
   Please choose whether or not to redirect HTTP traffic to HTTPS, removing HTTP access.
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   1: No redirect - Make no further changes to the webserver configuration.
   2: Redirect - Make all requests redirect to secure HTTPS access. Choose this for
   new sites, or if you're confident your site works on HTTPS. You can undo this
   change by editing your web server's configuration.
   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
   Select the appropriate number [1-2] then [enter] (press 'c' to cancel):
   ```

6. `systemctl restart apache2`
7. Edit these variables in `/etc/gitea/app.ini`

   ```
   ROOT_URL     = https://new.domain/
   SSH_DOMAIN   = new.domain
   ```

8. run `cerbot certificates` to check if everything is ok
9. delete the old certificate: `certbot delete --cert-name old.domain`
10. if you use a local DNS resolver like Unbound remember to update
    the domain records
