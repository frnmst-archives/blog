---
title: Fixing the /dev/md127 problem
tags: [tutorial, parabola, raid-partition, kernel naming, dev-md127, mdadm, mkinitcpio]
updated: 2021-09-13 17:05:00
description: Fixing a RAID array kernel name
---

A quick *how to* today.

`/dev/md127`. It happened again while I was adding a new RAID array to
replace another one. You have no idea the number of times I had to look on the
Internet on how to fix this problem. It usually take 1/2 hour to get the
correct instructions, and I always tell to myself:

> The solution to this problem is very simple. **I know I will remember how
> to deal with it the next time.**

But this is not the case, hence the reason for writing this post.

The fix is quite simple:

- Get the list of all raid arrays

      # mdadm --detail --scan

- Copy the array of interest from previous output. Remove the `name`
  variable and append what remains in `/etc/mdadm.conf`. If you have for
  example

      ARRAY /dev/md127 metadata=1.2 name=<hostname>:127 UUID=<some_uuid>

  just keep

      ARRAY /dev/md10 metadata=1.2 UUID=<some_uuid>

  Notice how `/dev/md127` changed to `/dev/md10` which is our desired device
  kernel name.

- Update the initramfs. In my case, on Parabola GNU/Linux-libre

      # mkinitcpio -p linux-libre
      # mkinitcpio -p linux-libre-lts

- Reboot and check if the RAID device name is now correct.

All this applies if you use mdadm and a GNU/Linux system.
