---
title: Hacking on the CSS of the Sphinx RTD's theme
tags: [rtd, read-the-docs, css, hacking, sphinx, theme]
updated: 2022-11-01 16:37:00
description: Override the CSS of the Sphinx RTD theme to improve navigation
images:
  - id: '0'
    filename: 'screencast.gif'
    alt: ! 'Hacking on the CSS'
    width: '512'
---

<!--excerpt_start-->
[Read The Docs' Sphinx theme](https://github.com/readthedocs/sphinx_rtd_theme)
(RTD) is quite good but there are some ways<!--excerpt_end--> to improve it.
Here are some useful CSS overrides I found. The *Navbar and sidebar* one I did it myself
but it's not fully functional.

## Enable CSS override

Add this content in your `conf.py` settings

```python
html_static_path = ['_static']
html_css_files = ['css/custom.css']
```

Create a file in the static directory

```shell
touch _static/css/custom.css
```

## CSS

### Content width

Increase content width. This is an problem for wide screens
since you get too much unused space IMHO. Also, this issue seems to be a trend
all over Internet.

See
[this StackOverflow question](https://stackoverflow.com/questions/23211695/modifying-content-width-of-the-sphinx-theme-read-the-docs).

```css
.wy-nav-content {
    max-width: 1536px !important;
}
```

where

```
1536px = (1024 + 512)px
```

### Tables

Responsive tables. See
[this GitHub issue](https://github.com/readthedocs/sphinx_rtd_theme/issues/117).

```css
/* Fix tables. */
.wy-table-responsive table td {
    white-space: normal;
}
```

### Navbar and sidebar

Use the mobile bar for bigger resolutions.
This makes the sidebar toggable with a button
and the navbar fixed on top when scrolling a page.

A [GitHub issue](https://github.com/readthedocs/sphinx_rtd_theme/issues/336)
is currenly open for hiding the sidebar.

Unfortunately when I toggle the sidebar using my CSS code the page scrolls
at the top because it points to a `div` or something.
Moreover the page gets unscrollable until I toggle the sidebar again.

{% include image.html id="0" %}

```css
@media screen and (min-width:769px) {
    .wy-nav-top {
        display: block;
        position: sticky;
        top: 0;
    }

    .wy-nav-side {
        left: -300px;
    }

    .wy-nav-side.shift {
        width: 300px;
        left: 0;
    }

    .wy-menu.wy-menu-vertical,
    .wy-side-nav-search,
    .wy-side-scroll {
        width: auto;
    }

    .wy-nav-content-wrap {
        margin-left: 0;
    }

    .wy-nav-content-wrap.shift {
        position: fixed;
        min-width: 100%;
        left: 300px;
        top: 0;
        height: 100%;
    }
}
```

### Sidebar

Hide the sidebar altogether. The problem with this approach is that you
lose the search textbox.

See [this GitHub issue](https://github.com/readthedocs/sphinx_rtd_theme/issues/987).

```css
.wy-nav-side {
    display: none;
}

.wy-nav-content-wrap {
  margin-left: 0;
}
```
