---
title: Learn Python unit tests in 5 minutes
tags: [python, unit-tests, video, youtube]
updated: 2023-07-18 16:20:00
description: Understand Python unit tests in 5 minutes through practical and real world examples
canonical_url: 'https://youtu.be/VExHrZGLahc'
---
In this video you will learn about unit tests in Python. We will answer these
three questions:

- what are unit tests?
- how do they work?
- how to write effective ones?

You'll see some real world examples as well.

<iframe width="560" height="315" src="https://www.youtube.com/embed/VExHrZGLahc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
