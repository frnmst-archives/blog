---
title: ! 'Invert letter cases of a string in C'
tags: [string, programming, tutorial, char, c, c-plus-plus, letter-case, algorithm, video, youtube]
updated: 2023-08-29 17:00:00
description: ! 'Invert the letter case of a string in C: algorithm explained as pseudocode and implemented in C and C++'
canonical_url: 'https://youtu.be/-9Tp3rB-5n0'
---
Here's a new video about programming in C. You'll see an exercise about inverting the letter cases of a string.

Various solutions are possible: you'll see the algorithm, an implementation in C and a different one in C++.

<iframe width="560" height="315" src="https://www.youtube.com/embed/-9Tp3rB-5n0?si=swSlxChUHKcY2U7J" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
