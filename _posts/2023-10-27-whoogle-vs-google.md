---
title: ! 'Whoogle vs Google'
tags: [search, whoogle, google, metasearch, search-engine, tutorial, video, youtube]
updated: 2023-10-27 17:00:00
description: ! 'Comparison between the metasearch engine Whoogle and Google'
canonical_url: 'https://youtu.be/ARB7S7Px35w'
---

In this video I compare the libre (FOSS) metasearch engine Whoogle against
Google. Whoogle is self-hostable, and does not contain tracking elemenents. It
acts as a _search proxy_ between your device and Google.

In the video you'll find the configuration for reverse-proxying Whoogle to the
Apache HTTP webserver.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ARB7S7Px35w?si=t6cMG9GPo3WMkIyv" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
