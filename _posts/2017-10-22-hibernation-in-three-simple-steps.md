---
title: How to enable hibernation in three simple steps
tags: [tutorial, hibernation, parabola, systemd, mkinitcpio, syslinux]
updated: 2021-09-13 14:07:00
description: How to enable hibernation on systemd and mkinitcpio-based systems
---

Greetings to all,

It has been some time since I needed to enable hibernation on my machine so
that I could find all my stuff exacly as I left it the previous time. This
is especially useful to me since I usually leave multiple terminals opened. So,
untill recently, I suspended instead of hibernate, but that is not
always practical.

What follows is a summary of instuctions I gathered from
[multiple](https://wiki.archlinux.org/index.php/Power_management/Suspend_and_hibernate)
[sources](https://wiki.archlinux.org/index.php/Mkinitcpio#Image_creation_and_activation)
and it reflects my use case and configuration.

## Prerequisites

- A swap partition with size >= RAM size.
- Systemd
- mkinitcpio
- A bootloader that supports kernel parameters.

## Step 1

Get the UUID of your swap partition.

    # UUID="$(blkid | grep swap | awk '{print $2}' | tr -d "\"")"

## Step 2

Edit the `/etc/mkinitcpio.conf` file to include `resume` after `udev` in the
`HOOKS` section. This is how mine looks:

    HOOKS="base udev mdadm_udev lvm2 resume autodetect modconf block filesystems keyboard fsck"

Then we must regenerate the ramdisk environment (please get the
correct kernel names first). For example:

    # mkinitcpio -p linux-libre
    # mkinitcpio -p linux-libre-lts

## Step 3

Copy the variable you gathered earlier.

    $ echo $UUID

Append the `resume` kernel parameter to your bootloader configuration. For
example, this would be a modified Syslinux entry to accomodate the new
parameter.

    LABEL <some label>
        MENU LABEL <some value>
        LINUX <some other value>
        APPEND <yet again other values> resume=<paste the $UUID variable here>
        INITRD <and again>

## Testing

    $ systemctl hibernate


That's it.

Enjoy.
