---
title: Add a remote shared printer with a couple of shell commands
tags: [tutorial, printer, cups-printing, share]
updated: 2021-10-25 17:03:00
description: How to add a remote shared printer within CUPS using a couple of shell commands
---

If you happen to have a USB printer without WiFi and a computer that will
act as server, then you can use [CUPS](https://www.cups.org/) to share it.

Once you setup CUPS to allow non-localhost connections, on port 631, you can
use the web interface for the configuration. At the time, I tried to add the
printer from a client computer using the web interface and selecting the
driver, just like I did on the server. I got a `Filter failed` error.
Basically, I think that the problem was that the file got through the printer
filters twice (see `$ man 7 filter`). To avoid this, the printer needs to be
added as *raw* from a CLI shell:

```shell
local_printer_name="Printer"
server_hostname="192.168.0.1"

# This must be the exact same name
# as the one reported on the server.
remote_printer_name="Remote_printer"

description="Remote printer"
location="Connected to the server"

lpadmin -p ${local_printer_name} -v \
ipps://${server_hostname}:631/printers/${remote_printer_name} \
-D "${description}" -L "${location}" -E
```

Please note that `lpadmin` needs root privileges.

The only thing to do is to edit the variables appropriately and then do a printer test.

Recently I experienced encryption certificate problems using `ipps://` but
everything worked fine using the plain `ipp://` protocol. If you are
in a trusted network this shouldn't be a problem.

You can now set the new printer as the default system one:

```shell
lpoptions -E -d ${local_printer_name}
```

Just like `lpadmin`, `lpoptions` also needs root privileges.

In case you need to remove the printer you have to run the following:

```shell
lpadmin -x ${local_printer_name}
```

See `$ man 8 lpadmin` and `$ man 1 lpoptions` for all possible parameters.

That's it...

## Note

This post is an adaptation of the original one at
[linuxdifficile](https://linuxdifficile.wordpress.com/2016/03/02/aggiungere-una-stampante-remota-condivisa-da-un-sistema-gnulinux-con-un-solo-comando/)
