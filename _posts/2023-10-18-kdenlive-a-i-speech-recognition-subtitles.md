---
title: ! 'Kdenlive A. I.  speech recognition subtitles'
tags: [kdenlive, ai, speech-recognition, tutorial, video, youtube]
updated: 2023-10-18 17:00:00
description: ! 'Set up speech reconition to generate subtitles in Kdenlive'
canonical_url: https://youtu.be/HiY6px4KYIc
---

[Kdenlive](https://kdenlive.org/en/) has a feature that enables you to get
subtitles via speech recognition.

In this video I show you how to setup the
[openai-whisper](https://github.com/openai/whisper) Python package so that you
can use it in Kdenlive's flatpak environment.

There are some caveats and you'll see my attemps while trying the speech
recognition on an existing video.

The most important thing is thast you can export the subtitles as an SRT file.

<iframe width="560" height="315" src="https://www.youtube.com/embed/HiY6px4KYIc?si=xis28tcoTQ6PvwcZ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
