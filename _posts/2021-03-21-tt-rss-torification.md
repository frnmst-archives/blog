---
title: Tiny Tiny RSS over TOR
tags: [tutorial, tt-rss, tor]
updated: 2022-10-08 15:33:00
description: Running TT-RSS over TOR
---

## Table of contents

<!--TOC-->

- [Table of contents](#table-of-contents)
- [Installation](#installation)
- [Hacking](#hacking)
- [Note](#note)
- [Footnotes](#footnotes)

<!--TOC-->

## Installation

<!--excerpt_start-->
1. install and configure TOR, Privoxy and TT-RSS<!--excerpt_end-->

   ```shell
   apt-get install tor privoxy tt-rss
   ```

2. configure Privoxy. Add this to `/etc/privoxy/config`

   ```conf
   forward-socks5 / 127.0.0.1:9050 .
   listen-address 127.0.0.1:8123
   ```

3. follow the [ArchWiki instructions](https://wiki.archlinux.org/index.php/TT-RSS) using MariaDB as backend.
   Also follow [these](https://wiki.archlinux.org/index.php/MariaDB#Installation) [instructions](https://wiki.archlinux.org/index.php/MariaDB#Improve_initial_security)
   to configure MariaDB
4. follow the install setup of TT-RSS
5. add this to you Apache virtual host

   ```apache
   DocumentRoot "/usr/share/tt-rss/www"
   <Directory "/usr/share/tt-rss/www">
       AllowOverride All
       Options FollowSymlinks
       php_admin_value open_basedir "/tmp/:/usr/share/tt-rss/www/lib/dijit:/usr/share/tt-rss/www/lib/dojo:/usr/share/tt-rss/www/lib/phpqrcode:/usr/share/tt-rss/www/lib/scriptaculous:/usr/share/tt-rss/www/include:/var/lib/tt-rss:/usr/share/tt-rss/www/lib:/usr/share/tt-rss/www:/usr/share/php:/etc/tt-rss:/var/log/tt-rss:/var/cache/tt-rss"
       Require all granted
   </Directory>

   Include /etc/letsencrypt/options-ssl-apache.conf
   SSLCertificateFile /etc/letsencrypt/live/my-domain.example.com/fullchain.pem
   SSLCertificateKeyFile /etc/letsencrypt/live/my-domain.example.com/privkey.pem
   ```

## Hacking

1. have a look at [https://binfalse.de/2015/05/06/ttrss-with-proxy/](https://binfalse.de/2015/05/06/ttrss-with-proxy/)[^1]. Since then the variables have changed and so here
   are some adaptations:

   1. `apt search tt-rss` and get the version:

      ```
      tt-rss/stable,now 18.12+dfsg-1.1 all [installed]
      Tiny Tiny RSS - web-based news feed (RSS/Atom) aggregator
      ```

   2. find a commit in the [source code](https://github.com/archlinux/svntogit-community/commits/packages/tt-rss/trunk) corresponsing to that version

      ```shell
git clone https://git.tt-rss.org/fox/tt-rss
cd tt-rss
git checkout 986ca251f995f7754a0470d3e0c44538a545081f       # This commit roughly corresponds to version 18.12 as identified earlier.
      ```

   3. search for `PROXY` strings:

      ```shell
grep -rn PROXY *
      ```

      which results in:

      ```php
include/functions.php:376:			if (defined('_HTTP_PROXY')) {
include/functions.php:377:				curl_setopt($ch, CURLOPT_PROXY, _HTTP_PROXY);
include/functions.php:484:			if (defined('_HTTP_PROXY')) {
include/functions.php:486:				$context_options['http']['proxy'] = _HTTP_PROXY;
plugins/af_unburn/init.php:40:				if (defined('_CURL_HTTP_PROXY')) {
plugins/af_unburn/init.php:41:					curl_setopt($ch, CURLOPT_PROXY, _CURL_HTTP_PROXY);
      ```


   5. add the following at the end of `/etc/tt-rss/config.php`. These variables should cover even more recent TT-RSS versions:

      ```php
define('HTTP_PROXY', '127.0.0.1:8123');
define('_HTTP_PROXY', '127.0.0.1:8123');
define('_CURL_HTTP_PROXY', '127.0.0.1:8123');
define('SELF_USER_AGENT', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0');
      ```

2. finally restart the services: `systemctl restart tt-rss apache2 privoxy`

## Note

*Please note that this page is deprecated and will only be kept for historical
reference. Have a look at
[https://docs.franco.net.eu.org/ftutorials/en/content/server/tt-rss.html#torification](https://docs.franco.net.eu.org/ftutorials/en/content/server/tt-rss.html#torification)
for an updated version.*

## Footnotes

[^1]: This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. Author: Martin Scharm
