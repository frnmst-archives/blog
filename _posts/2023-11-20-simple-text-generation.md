---
title: ! 'Simple text generation'
tags: [computer-recreations, python, textgeneration, video, youtube]
related_links: ['https://www.youtube.com/watch?v=rgMATFBacYA']
updated: 2023-11-20 18:00:00
description: ! 'A followup of the "Probability-based random text generation" video: lots of improvements'
canonical_url: 'https://www.youtube.com/watch?v=rgMATFBacYA'
---

Let's generate a mashup of texts from different sources (URLs) using
probability.

This video is an improvement of a previous one called
*[Probability-based random text generation](https://www.youtube.com/watch?v=XE00WkJ5sIo)*:
it uses much less memory.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rgMATFBacYA?si=_kdY469HAha5YMhs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
