---
title: Nextcloud's Recognize app
tags: [nextcloud, ai, artificial-intelligence, recognize, video, youtube]
updated: 2023-07-29 22:15:00
description: Set up Nextcloud's Recognize app with appropriate values to avoid excessive CPU usage
canonical_url: 'https://youtu.be/AP3YXkvGw_U'
---
In this video I show you how to setup the Recognize app in Nextcloud and what values to use to avoid eating up all the CPU.

<iframe width="560" height="315" src="https://www.youtube.com/embed/AP3YXkvGw_U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
