---
title: A new hosting for this blog
tags: [tutorial, hosting, jekyll, blog, gitlab]
updated: 2022-10-08 15:33:00
description: This blog is now self-hosted
---

<!--excerpt_start-->
Migrating from [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) (or *put any name here*)
to a private webserver has one main advantage: to be in control of *all its data*<!--excerpt_end-->.

## Problems

You have most certainly read or heard that self-hosting a website is a very bad idea because:

- it might pose security risks
- it is unreliable
- it is hard to maintain
- etc...

and all of that is true. For this specific website I don't care. It is *just* a blog...

## Howtos

Up until now I used a system called [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/). Every time I committed to the git repository
a Docker worker would rebuild the website. That usually took from 3 to 5 minutes!

Since this website is generated using [Jekyll](https://jekyllrb.com/)
I followed [this guide](https://jekyllrb.com/docs/deployment/automated/#git-post-receive-hook) but
created a new file called `hooks/post-receive` which is a little different to the one in the tutorial:

```shell
#!/usr/bin/bash -l
#
# The MIT License (MIT)
#
# Copyright (c) 2008-present Tom Preston-Werner and Jekyll contributors
#               2021 Franco Masotti
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

TMP_GIT_CLONE=""${HOME}"/tmp/myrepo"
PUBLIC_WWW='/var/www/blog.frnmst.duckdns.org'

# Install Ruby Gems to ~/gems
export GEM_HOME=""${HOME}"/gems"
export PATH="${GEM_HOME}"/bin:"${PATH}"
GEMFILE=""${TMP_GIT_CLONE}"/Gemfile"

git clone "${GIT_DIR}" "${TMP_GIT_CLONE}"
BUNDLE_GEMFILE="${GEMFILE}" bundle install
BUNDLE_GEMFILE="${GEMFILE}" bundle exec \
    jekyll build \
        --trace \
        --strict_front_matter \
        --verbose \
        --trace \
        --source "${TMP_GIT_CLONE}" \
        --destination "${PUBLIC_WWW}"

rm --recursive --force "${TMP_GIT_CLONE}"
```

Moreover, before running that git hook:

1. install ruby: `apt-get install ruby ruby-dev`
2. add the deploy user to the webserver running group. The deploy user must have
   access to the destination directory
3. login as the deploy user
4. add this to its `~/.profile`:

   ```shell
   export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
   export PATH="$PATH:$GEM_HOME/bin"
   ```

5. install bundle: `gem install bundle`

That's it.
