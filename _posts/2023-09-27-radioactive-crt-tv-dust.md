---
title: ! 'Radioactive CRT TV dust'
tags: [radon, geiger-counter, radiation, video, youtube]
updated: 2023-09-27 17:15:00
description: ! 'CRT monitors collect radioactive dust originating from decaying Radon progeny'
canonical_url: 'https://youtu.be/Uc-1ej7E4Qg'
---

Radioactive CRT TV dust

<iframe width="560" height="315" src="https://www.youtube.com/embed/Uc-1ej7E4Qg?si=ytpdAR-bNouqLaKM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
