---
title: ! 'Computer recreations: random numbers and properties'
tags: [computer-recreations, pi, monte-carlo-method, galton-board, tutorial, video, youtube]
updated: 2023-09-22 17:00:00
description: ! 'A new video for the "Computer recreations" playlist concentrating on practical examples of random numbers'
canonical_url: 'https://youtu.be/DgHp3FRw0Qk'
---

This new video of the "Computer recreations" playlist talks about random numbers and their properties. We'll see 3 of 5 examples explained in the article. All source code is written in Python.

The article is called "Five easy pieces for a do loop and random-number generator" by A. K. Dewdney published on the "Scientific American" magazine from April 1985.

<iframe width="560" height="315" src="https://www.youtube.com/embed/DgHp3FRw0Qk?si=_0PmxSDxaJoor5Lg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
