---
title: ! 'Collabora online in Nextcloud'
tags: [nextcloud, collabora-office, collabora, office, server, docker, docker-compose, apache, video, youtube]
updated: 2023-09-05 17:00:00
description: ! 'Setup Collabora online to be used with Nextcloud as a Docker container'
canonical_url: 'https://youtu.be/bOBZ5orsV9g'
---
Collabora is a cloud office suite that can be integrated in Nextcloud.

In this new video of the Nextcloud playlist I show you how to setup the Collabora online server and its app integration in Nextcloud. Instead of using the "built-in" Collabora server we'll use a separate one for performance reasons.

There are a few caveats but the setup is almost straight forward.

<iframe width="560" height="315" src="https://www.youtube.com/embed/bOBZ5orsV9g?si=bsitIi2FHyVyYfA8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
