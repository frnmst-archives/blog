---
title: My Python release workflow
tags: [python, git, workflow, aur, arch]
updated: 2023-02-03 22:00:28
description: A personal reminder with the instructions for releasing new versions of Python packages
---

This is a personal reminder for the operations to do
in case of a new version release with some of my Python repositories.

<!--more-->

*Please note that this page is deprecated and will only be kept for historical
reference. Have a look at the
[workflow](https://docs.franco.net.eu.org/ftutorials/en/content/programming/python/workflow.html)
page for an updated version.*

## Rules

- Use [md_toc](https://software.franco.net.eu.org/frnmst/md-toc) as an example blueprint.
- Assume that the root of the repository is `./`.
- Variables are marked with braces and the dollar sign, e.g: `${variable}`.
- `${MAJOR}`, `${MINOR}` and `${PATCH}` refer to the varaibles reported on the [Semantic Versioning](https://semver.org/#summary) document.
- 3.5 <= Python version < 4
- commits and tags must be signed; run these commands once per repository:

      git config commit.gpgsign true
      git config user.signingkey ${gpg_signing_key}

## Workflow

1. go to the project's directory
   1. `cd ${project_name}`

2. check that the current branch is not `master`
   1. `[ "$(echo git branch | grep "*" | tr -d '* ')" = "master" ] && echo "STOP" || echo "OK"`

3. finish working on the development branch, `${dev_branch}`, and commit
   1. if needed, create a new [asciinema](https://asciinema.org/) demo file and upload it
      1. `cd ./asciinema`
      2. if there have been changes from the previous version
         1. `touch ${project_name}_asciinema_${MAJOR}_${MINOR}_${PATCH}_demo.sh`
         2. modify the `${project_name}_asciinema_${MAJOR}_${MINOR}_${PATCH}_demo.sh` file accordingly
         3. `asciinema rec --command=./${project_name}_asciinema_${MAJOR}_${MINOR}_${PATCH}_demo.sh ${project_name}_asciinema_${MAJOR}_${MINOR}_${PATCH}.json`
         4. `asciinema play ${project_name}_asciinema_${MAJOR}_${MINOR}_${PATCH}.json`
         5. `asciinema upload ${project_name}_asciinema_${MAJOR}_${MINOR}_${PATCH}.json`
         6. edit the `./README.rst` file with the new asciinema link
      3. OTHERWISE, if there have been no significant changes from the previous version
         1. `ln -s ${project_name}_asciinema_${MAJOR}_${OLD_MINOR}_${OLD_PATCH}_demo.sh ${project_name}_asciinema_${MAJOR}_${MINOR}_${PATCH}_demo.sh`
         2. `ln -s ${project_name}_asciinema_${MAJOR}_${OLD_MINOR}_${OLD_PATCH}.json ${project_name}_asciinema_${MAJOR}_${MINOR}_${PATCH}.json`
   2. `git add -A`
   3. `git commit -m "${commit_message}"`
   4. `git push`

4. if necessary, update version numbers and requirements
   1. update version numbers in:
    - `./setup.py`
    - `./docs/conf.py`
    - all downstream distribution packages (see the `./packages` directory)
   2. update the `./Pipfile` with the appropriate packages
   3. `make install-dev`
   4. `make doc`
   5. `make pep`
   6. `make test`
   7. `make install`
   8. `cd ~ && python -c 'import ${package_name}' && cd ${OLDPWD}`
   9. `make uninstall`
   10. `make clean`
   11. update copyright years, emails and contributors in:
     - `./README.rst`
     - `./docs/conf.py`
     - `./docs/copyright_license.rst`
     - all Python source files
     - all downstream distribution packages (see the `./packages` directory)

       update changed files only

   12. `git add -A`
   13. `git commit -m "Preparing for new release."`
   14. `git push`

5. update the documentation
   1. `make clean && make doc`
   2. `rm -rf ~/html && cp -aR docs/_build/html ~`
   3. `git checkout gh-pages`
   4. `rm -rf _modules _sources _static _images`
   5. `mv ~/html/{*,.nojekyll,.buildinfo} .`
   6. `git add -A`
   7. `git commit -m "New release."`
   8. `git push`

6. merge the `${dev_branch}` branch into `master` and create a new annotated git tag
   1. `git checkout master`
   2. `git merge ${dev_branch}`
   3. `git tag -s -a ${version_id} -m "Some sensible comments highlighting relevant changes from the previous release."`
   4. `git push`
   5. `git push origin ${version_id}`

7. upload the package to [PIP](https://pypi.org/)
   1. `make clean`
   2. `make dist`
   3. `make upload`

8. upload the package on the [software]({{ site.rooturl}}/software/) page:
   1. follow the instructions reported [here]({{ site.rooturl}}/software/#upload)

9. update downstream distribution packages
   1. [AUR](https://wiki.archlinux.org/index.php/Arch_User_Repository)
      1. copy `./packages/aur/PKGBUILD` file in the project's AUR git directory (`${projects_aur_git_directory}`)
      2. copy the signature file in the project's AUR git directory (`${projects_aur_git_directory}`)
      3. go to the project's AUR git directory
         1. `cd ~/${projects_aur_git_directory}`
      4. update the sha512 checksum in the `PKGBUILD` file with the one in the [software]({{ site.rooturl}}/software/) page
      4. test the changes
         1. `makepkg -rsi`
         2. remove all the build files and the installed package
            1. `rm -rf pkg src *.tar.*`
            2. `pacman -Rnus ${pacman_package_name}`
      5. update and push
         1. `makepkg --printsrcinfo > .SRCINFO`
         2. `git add PKGBUILD .SRCINFO`
         3. `git commit -m "New release."`
         4. `git push`

10. if needed, update the entry on the
   [Free Software Directory](https://directory.fsf.org/wiki/Main_Page)
