---
title: ! 'Serve Nextcloud with TOR hidden service'
tags: [nextcloud, tor, apache, tutorial, video, youtube]
updated: 2023-10-14 17:00:00
description: ! 'Set up and use your Nextcloud instance via a TOR hidden service'
canonical_url: https://youtu.be/g8ge1jB7mik
---

To connect to your Nextcloud instance via TOR all you have to do is to:

- create a new TOR hidden service
- create a new virtual host and change some HTTP headers
- whitelist the onion domain in the `config.php` Nextcloud file

In this video I show you all the necessary steps to set this up. You'll also
see what works and what does not when you use Nextcloud via TOR.

<iframe width="560" height="315" src="https://www.youtube.com/embed/g8ge1jB7mik?si=r8pAXr02rIRneRE5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
