---
title: An alternative to GitHub Pages
tags: [tutorial, hosting, gitlab, github, github-pages]
updated: 2022-10-08 15:33:00
description: A DIY alternastive to GitHub and GitLab pages
---

<!--excerpt_start-->
Following what I have written in a [previous post]({% post_url 2021-02-17-new-hosting %}) I present you a DIY alternative to GitHub and GitLab pages<!--excerpt_end-->.

I created a `docs.` virual host in Apache. Each repository documentation is contained in a distinct directory
under that domain (and directory). For example [md-toc](https://software.franco.net.eu.org/frnmst/md-toc/) is under [docs.franco.net.eu.org/md-toc/](https://docs.franco.net.eu.org/md-toc/)

The first time you set up the pages do the following:

1. create the bare repository like in the [previous post]({% post_url 2021-02-17-new-hosting %})
2. change the default branch in the `HEAD` file from `ref: refs/heads/master` to `ref: refs/heads/docs`
3. mkdir the destination directory
4. create a new executable file in `hooks/post-receive`, like this:

   ```shell
   #!/usr/bin/bash -l
   #
   # The MIT License (MIT)
   #
   # Copyright (c) 2008-present Tom Preston-Werner and Jekyll contributors
   #               2021 Franco Masotti
   #
   # Permission is hereby granted, free of charge, to any person obtaining a copy
   # of this software and associated documentation files (the "Software"), to deal
   # in the Software without restriction, including without limitation the rights
   # to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   # copies of the Software, and to permit persons to whom the Software is
   # furnished to do so, subject to the following conditions:
   #
   # The above copyright notice and this permission notice shall be included in all
   # copies or substantial portions of the Software.
   #
   # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   # IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   # FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   # AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   # LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   # OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
   # SOFTWARE.

   TMP_GIT_CLONE=""${HOME}"/tmp/myrepo"
   PUBLIC_WWW='/var/www/docs.franco.net.eu.org/myrepo'

   git clone "${GIT_DIR}" "${TMP_GIT_CLONE}"
   rm -rf "${PUBLIC_WWW}"/*
   cp -aR "${TMP_GIT_CLONE}"/* "${PUBLIC_WWW}"
   rm -rf "${TMP_GIT_CLONE}"
   exit
   ```

As you can see these instructions are not very different from this [blog post from GitLab](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/#add-gitlab-ci).

Finally follow the usual standards, starting from
[this version](https://software.franco.net.eu.org/frnmst/fpydocs/raw/commit/bd60eb940cb3d82654e3085e7d51ddffb69cc00f/docs/workflow_common.rst).
