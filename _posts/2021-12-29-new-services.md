---
title: New services
tags: [services, mirror, outage]
updated: 2022-10-08 15:33:00
description: New services available on this server.
---

<!--excerpt_start-->
- an HTTP Debian GNU/Linux mirror is available<!--excerpt_end--> for Bullseye and Sid at
  [debian.franco.net.eu.org](https://debian.franco.net.eu.org/).

- outage monitoring is available at
  [outage.franco.net.eu.org](https://outage.franco.net.eu.org/).
  You can subscribe to the available RSS feeds.

Enjoy
