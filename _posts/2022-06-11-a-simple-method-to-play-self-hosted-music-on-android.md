---
title: A simple method to play self-hosted music on Android
tags: [tutorial, vlc, connectbot, ssh, android, music, sftp]
updated: 2022-10-08 15:33:00
description: ! 'My setup to be able to listen to self-hosted music using a mobile phone'
images:
  - id: '0'
    filename: 'connectbot_port_forwarding.jpg'
    alt: 'connectbot port forwarding'
    caption: 'ConnectBot port forwarding'
---

## Introduction

<!--excerpt_start-->
Here is simple way to listen to self-hosted music on Android
without installing specific streaming software<!--excerpt_end--> on your server.

This method relies 100% on SSH.

## Table of contents

<!--TOC-->

- [Introduction](#introduction)
- [Table of contents](#table-of-contents)
- [Software needed](#software-needed)
  - [GNU/Linux](#gnulinux)
  - [Android](#android)
- [Configuration](#configuration)
  - [Introduction](#introduction-1)
  - [New user](#new-user)
  - [OpenSSH](#openssh)
  - [ConnectBot](#connectbot)
    - [Key pair generation](#key-pair-generation)
    - [Add a new host to ConnectBot](#add-a-new-host-to-connectbot)
    - [Test](#test)
    - [Port forwarding](#port-forwarding)
  - [VLC](#vlc)
- [Final testing](#final-testing)
- [Problems](#problems)

<!--TOC-->

## Software needed

### GNU/Linux

- [OpenSSH](https://www.openssh.com/portable.html)

### Android

Install these apps from [F-Droid](https://f-droid.org/)

- [Connectbot](https://f-droid.org/en/packages/org.connectbot/)
- [VLC](https://f-droid.org/en/packages/org.videolan.vlc/)

## Configuration

### Introduction

At the time of writing VLC for Android does not support public key authentication with
SFTP. To solve this problem, we will use ConnectBot to:

- connect to the server
- make a local SSH tunnel for VLC

The following instructions apply to a GNU/Linux system.

### New user

Connect to the server.

For sake of security I created a new user called `music`:

```shell
useradd -m -s /bin/bash music
passwd music
```

I then moved all the music files in a directory called `/home/music/files`.

### OpenSSH

To further harden the server in terms of security we must disable TTYs and force the
internal SFTP server for the SSH daemon. Here is what I added to `/etc/ssh/sshd_config`:

```conf
Match User music Address 127.0.0.1
    PasswordAuthentication yes
    IPQoS throughput
    PermitTTY no
    ForceCommand internal-sftp

Match User music
    X11Forwarding no
    AllowTcpForwarding yes
    PermitTTY no
    ForceCommand internal-sftp
    IPQoS throughput
```

Now, restart OpenSSH

```shell
systemctl restart ssh
```

### ConnectBot

#### Key pair generation

Open ConnectBot on your phone and generate a key pair:

1. tap the menu icon, in the top right of the screen
2. *Manage Pubkeys*
3. tap the `[+]` button
4. set these values
   - *Nickname*: `myserver`
   - *Type*: `RSA`
   - *Bits*: `4096`
   - *Password*: \<a password\>
   - *Load key on start*: true

The encryption password will be prompted unless it has not been previously unlocked,
for example by a previous connection.

Unlock the key by tapping it. Tap the key longer and select *Copy public key*.

You now have to copy the public key to the server (`/home/music/.ssh/authorized_keys`).
You can send it by email for example (it is now in Android's copy-paste buffer).

#### Add a new host to ConnectBot

Now we need to add an SSH host on ConnectBot. Go back to Connectbot's home screen
and tap on the `+` button. Add the host and tap *Use pubkey authentication*.

Enable these flags:

- *Compression*
- *Stay connected*

Disable this flag:

- *Start shell session*

Set the `myserver` public key by tapping *Use pubkey authentication*.

#### Test

Disconnect from the server and reconnect to it. If everything works as
expected you won't see any errors.

#### Port forwarding

Make a long tap on the host and select *Edit port forwards* and then
tap the `+` button.

To be able for VLC to connect to the server we need to create a port forward, like this:

- *Nickname*: `vlc`
- *Type*: `Local`
- *Source Port*: `2222`
- *Destination*: `127.0.0.1:22`

{% include image.html id="0" %}

### VLC

Open the VLC app.

Go to the *Browse* section and tap the `+` button. Select `SFTP` as protocol and
edit the fields like this:

- *Port*: `2222`
- *Server address (domain name)*: `127.0.0.1`
- *Username*: `music`
- *Folder path (optional)*: `/home/music/files`
- *Server name, for conveniency*: `MUSIC`

Once you add the server tap the new directory icon you find in VLC *Browse* page.
The first time you do this you will be prompted for the `music` user's password.

You should now be able to see and play the music files.

## Final testing

Close ConnectBot and VLC and retry everything.

## Problems

- Not bandwidth efficient.
- No transcoding is possible using this method.
