---
title: The repositories are available on TOR
tags: [repositories, software, tor, privacy-policy, cookie-policy]
updated: 2021-09-13 17:05:00
description: The repositories are available on TOR.
---

The [Gitea instance containing my repositories](https://software.franco.net.eu.org) is now available on TOR: [jb2xdimn4cuoty3igqh3qknyt7ls6hwswzwl7xmieheemsjuirvqcaad.onion](http://jb2xdimn4cuoty3igqh3qknyt7ls6hwswzwl7xmieheemsjuirvqcaad.onion)

I have added a [privacy and cookie policy](https://software.franco.net.eu.org/frnmst/software.franco.net.eu.org/src/branch/master/privacy_policy.md#user-content-table-of-contents): [jb2xdimn4cuoty3igqh3qknyt7ls6hwswzwl7xmieheemsjuirvqcaad.onion/frnmst/software.franco.net.eu.org/src/branch/master/privacy_policy.md#user-content-table-of-contents](http://jb2xdimn4cuoty3igqh3qknyt7ls6hwswzwl7xmieheemsjuirvqcaad.onion/frnmst/software.franco.net.eu.org/src/branch/master/privacy_policy.md#user-content-table-of-contents)
