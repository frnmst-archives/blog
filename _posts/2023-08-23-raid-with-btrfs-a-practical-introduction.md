---
title: ! 'RAID with Btrfs: a practical introduction'
tags: [raid, btrfs, raid-1, raid-10, mdadm, video, youtube]
updated: 2023-08-23 17:00:00
description: Setting up, expanding and shrinking a RAID 1 array using Btrfs's embedded tools instead of mdadm
canonical_url: 'https://youtu.be/B88jeH5j88w'
---
In this video I show you an overview of how the RAID 1 level works in Btrfs. We'll see some of the basic operations such as creating, expanding and shrinking a RAID 1 array.

Using the embedded RAID levels of Btrfs is different from mdadm: there are pros and cons. Watch the video to learn more!

<iframe width="560" height="315" src="https://www.youtube.com/embed/B88jeH5j88w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
