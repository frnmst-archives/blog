---
title: Cloudpasta a Nextcloud instance you can try
tags: [nextcloud, cloudpasta, docker]
updated: 2023-12-01 00:00:00
description: Try Cloudpasta, a community Nextcloud instance.
---

*Update: Cloudpasta is not available anymore*

## Introduction

<!--excerpt_start-->
[Nextcloud](https://nextcloud.com/) is a set of client-server programs for file hosting
and collaboration<!--excerpt_end-->. A web UI and clients are available for several operating systems.

Here is a video explaining Nextcloud:

<iframe width="640" height="480" src="https://yewtu.be/embed/cxFQ24IlXcU"></iframe>

## Cloudpasta

You can try my Nextcloud instance called [Cloudpasta](https://cloudpasta.franco.net.eu.org) by
[registering](https://cloudpasta.franco.net.eu.org/apps/registration/). It's free up to 5 GB
as you can see from the table here:

| Name  | Size                  | Price          |
|-------|-----------------------|----------------|
| Free  | 0 B < size <= 5 GB    | free           |
| 50GB  | 0 B < size <= 50 GB   | 1.5 € / month  |
| 75GB  | 0 B < size <= 75 GB   | 2.0 € / month  |
| 100GB | 0 B < size <= 100 GB  | 2.5 € / month  |

[Terms and privacy policy](https://software.franco.net.eu.org/frnmst/cloudpasta.franco.net.eu.org#table-of-contents)

## Deploy your own Nextcloud

Nextcloud server and client software are all free (as in freedom)
so you can also setup your own instance. You can follow
[my instructions on ftutorials](https://docs.franco.net.eu.org/ftutorials/en/content/server/nextcloud.html).

## Encryption

I strongly suggest to encrypt your files if you are not the owner of
the instance you are using. More of this is explained in the
[advices](https://software.franco.net.eu.org/frnmst/cloudpasta.franco.net.eu.org#user-content-advices).
