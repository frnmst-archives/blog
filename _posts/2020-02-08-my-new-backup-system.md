---
title: My new backup system
tags: [tutorial, bash, shell, backup, rsync, luks, cryptsetup, sync, shred, borgmatic, borgbackup]
updated: 2022-10-08 15:33:00
description: An update on a previous post about backups
---

## Introduction

<!--excerpt_start-->
Last year [I wrote a post]({% post_url 2019-02-23-my-backup-system %})
about my backup system<!--excerpt_end-->.

In the meantime some things have changed:

- the main backups now use [borgmatic](https://torsion.org/borgmatic/) and [custom scripts](https://docs.franco.net.eu.org/automated-tasks/scripts.html#borgmatic-hooks-py) along with it
- **the encrypted backups are just a mirror of the main backups**
  - you will need [BorgBackup](https://www.borgbackup.org/) to access them
- i now use Btrfs instead of ext4 for the encrypted backups
  - use `mkfs.btrfs` intead of `mkfs.ext4`
- the partitioning scheme
  - unencrypted backups are pushed to a central computer
  - see the example cofiguration file below

and other did not:

- most of the initial steps

### Steps

1. install [Rsync](https://rsync.samba.org/),
   [Cryptsetup](https://gitlab.com/cryptsetup/cryptsetup/),
   and [GNU Bash](http://www.gnu.org/software/bash/bash.html).

2. follow the steps related to the encrypted backups [in this previous post]({% post_url 2019-02-23-my-backup-system %}).

## The new encrypted backup script

```shell
#!/usr/bin/env bash
#
# backup_enc.sh
#
# Copyright (C)  2019-2020  Franco Masotti <franco.masotti@live.com>.
# Permission is granted to copy, distribute and/or modify this document
# under the terms of the GNU Free Documentation License, Version 1.3
# or any later version published by the Free Software Foundation;
# with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
# A copy of the license is included in the section entitled "GNU
# Free Documentation License".

#
# This backup is intended to be run manually.
#

set -euo pipefail

CONFIG="${1}"

. "${CONFIG}"

[ ${UID} -eq 0 ]
cryptsetup open "/dev/disk/by-uuid/"${UUID}"" "${MAPPER_NAME}"
mount /dev/mapper/"${MAPPER_NAME}" "${DST}"
set +e
rsync --verbose --archive --acls --xattrs --hard-links --delete "${SRC}"/* "${DST}"
set -e
sync
umount "${DST}"
cryptsetup close "${MAPPER_NAME}"
```

### Configuration file

Create a configuration file for every backup.

You must put the correct UUIDs of the partition in the configuration file.
Copy the appropriate one from:

    $ lsblk -o name,uuid

This is an example for the `root` mountpoint of `host one`:

```shell
#
# backup_enc.hostone_root.conf
#
# Copyright (C)  2019-2020  Franco Masotti <franco.masotti@live.com>.
# Permission is granted to copy, distribute and/or modify this document
# under the terms of the GNU Free Documentation License, Version 1.3
# or any later version published by the Free Software Foundation;
# with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
# A copy of the license is included in the section entitled "GNU
# Free Documentation License".

UUID='<put the uuid here>'
MAPPER_NAME='hostone_root_enc'
SRC='/mnt/backups/hostone_root'
DST='/mnt/backups_enc/hostone_root'
```

## First backups

Once you have everything in place you may start the backup as root:

    # ./backup_enc.sh ./backup_enc.hostone_root.conf

~

Enjoy!
