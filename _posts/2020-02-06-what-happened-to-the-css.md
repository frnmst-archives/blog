---
title: What happened to the CSS?
tags: [css, website, blog]
updated: 2022-10-08 15:33:00
description: Reasons for the removal of the CSS from this website
---

<!--excerpt_start-->
Yes, I practically removed all the CSS file from<!--excerpt_start--> this website!

This gives me some advantages:

- I can now concentrate more on the structure and on the content
- I don't spend my time looking for solutions to do x, y or z in CSS
- the website is faster to load

~~If you have problems because of the absence of a dark theme, don't complain: install [Redshift](http://jonls.dk/redshift/).~~

Update 2022-07-21: now I'm using [GitHub's primer Jekyll theme](https://github.com/pages-themes/primer/) CSS because
it's more responsive and more elegant.

Update 2022-10-08: a dark theme has been implemented with < 10 CSS lines. You need to
set your device system settings in *dark mode*, or you can toggle dark mode using
the browser's development console.

```css
/* set the background color to black when client requests dark mode and invert the colors
 * from post by user lewisl9029: https://news.ycombinator.com/item?id=26472246
 * See also https://github.com/pages-themes/primer/issues/64
 */
@media (prefers-color-scheme: dark) {
    body {
        background-color: black;
        filter:  brightness(.9) contrast(1.2) hue-rotate(180deg) invert(80%);
    }
  img {
    filter: brightness(.6) contrast(1.2) invert(-80%) grayscale(50%);
  }
}
```

~

Cheers!
