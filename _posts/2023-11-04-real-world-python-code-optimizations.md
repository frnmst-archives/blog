---
title: ! 'Real-world Python code optimizations'
tags: [markdown, md-toc, python, programming, code, tutorial, video, youtube]
updated: 2023-11-04 18:00:00
description: ! 'Technical discussion of the latest changes for md-toc 8.2.1'
canonical_url: 'https://youtu.be/6QaW4qv7y1o'
---

Assignment expression, list and dict comprehensions, hashing the dict keys for
memory optimization: all these aspects have been applied to the
[md-toc]({{ site.rooturl }}/software/#md-toc) project and discussed in detail
in this video.

You'll see my discussion with me peaking at the official Python documentation
and some QAs on StackOverflow.

<iframe width="560" height="315" src="https://www.youtube.com/embed/6QaW4qv7y1o?si=gHZztwa4MIzaovi2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
