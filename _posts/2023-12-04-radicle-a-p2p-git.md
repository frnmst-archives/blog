---
title: ! 'Radicle: a P2P GIT'
tags: [radicle, git, p2p, decentralized]
updated: 2023-12-04 17:48:00
description: ! "Exploring Radicle: a P2P system to share GIT repositories"
---

I started exploring this software called [radicle](https://radicle.xyz/) which
is a peer to peer GIT system. Some of its purposes are to fight censorship
and to have a decentralized backup: the repositories, in-fact, live inside peer
nodes instead of centralized servers. All you need is an existing GIT
repository and the radicle program.

<!--TOC-->

- [Cloning a repo](#cloning-a-repo)
- [You can manage issues as well](#you-can-manage-issues-as-well)
- [GIT remotes](#git-remotes)
- [Conclusions](#conclusions)

<!--TOC-->

The basic documentation I found is from the `rad help` command.

After installing radicle you must create a new identity and start the node. To
improve the experience you should set up
[`ssh-agent`](https://wiki.archlinux.org/title/SSH_keys#SSH_agents).


```shell
rad auth
rad node start
```

## Cloning a repo

You can now start cloning repositories. To do this you just need to know the
repository identifier (RID), which is a specific type of
[Uniform Resource Name](https://en.wikipedia.org/wiki/Uniform_Resource_Name).

For example, I uploaded the [md-toc]({{ site.rooturl }}/software/#md-toc)
project to the network and its RID is `rad:z3rw93C1rVFE9wvSPcXBTo8UT8i7a`.

This is what happened after I cloned md-toc through radicle on another host:

```
rad clone rad:z3rw93C1rVFE9wvSPcXBTo8UT8i7a
✓ Passphrase:
✓ Unsealing key...
✓ Tracking relationship established for rad:z3rw93C1rVFE9wvSPcXBTo8UT8i7a with scope 'all'
✓ Fetching rad:z3rw93C1rVFE9wvSPcXBTo8UT8i7a from z6MkrLM…ocNYPm7..
✓ Forking under z6Mkmcu…JgVNHNx..
✓ Creating checkout in ./md-toc..
✓ Remote z6Mkrhm8eXtjbZK5J7Laz9fW5Z2dBcDZWRwtyGdapycvJZww added
✓ Remote-tracking branch z6Mkrhm8eXtjbZK5J7Laz9fW5Z2dBcDZWRwtyGdapycvJZww/master created for z6Mkrhm…ycvJZww
✓ Repository successfully cloned under /home/vm/md-toc/
╭────────────────────────────────────────────────────────────────────────────────╮
│ md-toc                                                                         │
│ Automatically generate and add an accurate table of contents to markdown files │
│ 0 issues · 0 patches                                                           │
╰────────────────────────────────────────────────────────────────────────────────╯
Run `cd ./md-toc` to go to the project directory.
```

## You can manage issues as well

As you can see, you can also manage issues and patches within the radicle
repository itself. If you clone the
[heartwood repository](https://app.radicle.xyz/nodes/seed.radicle.xyz/rad:z3gqcJUoA1n9HaHKufZs5FCSGazv5),
part of the radicle project, and list the issues you get this print:

```
rad issue

╭──────────────────────────────────────────────────────────────────────────────────────────────╮
│ ●   ID        Title                                                                          │
├──────────────────────────────────────────────────────────────────────────────────────────────┤
│ ●   696fb92   Sync fails immediately after adding delegate (before delegate has created for… │
│ ●   50f0fcf   Long-lived tokens support in httpd, for machine-to-machine connections         │
│ ●   2e51b37   radicle: repository traits                                                     │
╰──────────────────────────────────────────────────────────────────────────────────────────────╯
```

## GIT remotes

On the original repository, the one I initialized to be added on the radicle
network, I have these two GIT remotes:

```
git remote -v

# normal GIT remotes here

rad	rad://z3rw93C1rVFE9wvSPcXBTo8UT8i7a (fetch)
rad	rad://z3rw93C1rVFE9wvSPcXBTo8UT8i7a/z6Mkrhm8eXtjbZK5J7Laz9fW5Z2dBcDZWRwtyGdapycvJZww (push)
```

The RID (Repository identifier) is `z3rw93C1rVFE9wvSPcXBTo8UT8i7a`,
while the NID (Node Id) is `z6Mkrhm8eXtjbZK5J7Laz9fW5Z2dBcDZWRwtyGdapycvJZww`

The NID can be obtained with the `rad self` command.

```
rad self | grep NID

└╴Node ID (NID) z6Mkrhm8eXtjbZK5J7Laz9fW5Z2dBcDZWRwtyGdapycvJZww
```

To get the RID, just do

```
rad .

rad:z3rw93C1rVFE9wvSPcXBTo8UT8i7a
```

so the rad remote is described as `rad://${RID}/${NID}`

On the other machine where I cloned the repo, I have an extra remote:

```
rad	rad://z3rw93C1rVFE9wvSPcXBTo8UT8i7a (fetch)
rad	rad://z3rw93C1rVFE9wvSPcXBTo8UT8i7a/z6MkmcuJSPSxnKXogwzMnJCJpAwQXL4M42ZRnzjmKJgVNHNx (push)
vm@z6Mkrhm8eXtjbZK5J7Laz9fW5Z2dBcDZWRwtyGdapycvJZww	rad://z3rw93C1rVFE9wvSPcXBTo8UT8i7a/z6Mkrhm8eXtjbZK5J7Laz9fW5Z2dBcDZWRwtyGdapycvJZww (fetch)
vm@z6Mkrhm8eXtjbZK5J7Laz9fW5Z2dBcDZWRwtyGdapycvJZww	rad://z3rw93C1rVFE9wvSPcXBTo8UT8i7a/z6Mkrhm8eXtjbZK5J7Laz9fW5Z2dBcDZWRwtyGdapycvJZww (push)
```

The first remote is a local copy of the repository itself. The second one,
however, corresponds to the node that has created the repository. In-fact
I used the alias `vm` on the original node:

```
rad self | grep Alias

Alias           vm
```

There is also a radicle command to manage remotes. On the original node:

```
rad remote list

rad (canonical upstream)                             (fetch)
rad z6Mkrhm8eXtjbZK5J7Laz9fW5Z2dBcDZWRwtyGdapycvJZww (push)
```

On the other node:

```
rad remote list

rad                                                 (canonical upstream)                             (fetch)
rad                                                 z6MkmcuJSPSxnKXogwzMnJCJpAwQXL4M42ZRnzjmKJgVNHNx (push)
vm@z6Mkrhm8eXtjbZK5J7Laz9fW5Z2dBcDZWRwtyGdapycvJZww z6Mkrhm8eXtjbZK5J7Laz9fW5Z2dBcDZWRwtyGdapycvJZww (fetch)
```

where `z6MkmcuJSPSxnKXogwzMnJCJpAwQXL4M42ZRnzjmKJgVNHNx` is the NID of the
second node (just compare the last 2 letters: `Nx` vs `ww`)

```
rad self | grep NID

└╴Node ID (NID) z6MkmcuJSPSxnKXogwzMnJCJpAwQXL4M42ZRnzjmKJgVNHNx
```

I think this means that you can directly pull from the original node and you
can only directly push to the local rad storage, which is a fork of the
original repository. The `rad help` in-fact states:

```
Under the hood, the clone command does a few important things:

[ ... ]

- It creates a fork of the repository that is under your public key.
- It creates a rad remote which you can push to, to update your fork.
```

This schema, which you can find in the
[Radicle Improvement Proposals #1](https://app.radicle.xyz/nodes/seed.radicle.xyz/rad:z3trNYnLWS11cJWC6BbxDs5niGo82/tree/0001-heartwood.md)
document, makes all this somewhat clearer:

```
┌╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴┐          ┌╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴┐
┆ ┌───────────────────┐ ┌──────┐ ┆          ┆ ┌──────┐ ┌─────────────────┐ ┆
┆ │ Storage           │ │      │ ┆   Git    ┆ │      │ │ Storage         │ ┆
┆ │                   ├╸┆╸╸╸╸╸╸┆╸╸╸╸╸╸╸╸╸╸╸╸╸╸┆╸╸╸╸╸╸┆╸┤                 │ ┆
┆ │ ┌──────┐ ┌─────┐ ┌│ │      │ ┆ protocol ┆ │      │ │ ┌─────┐ ┌─────┐ │ ┆
┆ │ │repo  │ │repo │ ││ │      │ ┆          ┆ │      │ │ │repo │ │repo │ │ ┆
┆ │ ├──────┤ ├─────┤ ├│ │      │ ┆          ┆ │      │ │ ├─────┤ ├─────┤ │ ┆
┆ └─┴───╿──┴─┴───┬─┴─┴┘ │      │ ┆          ┆ │      │ └─┴───┬─┴─┴───╿─┴─┘ ┆
┆       │        │      │      │ ┆  gossip  ┆ │      │       │       │     ┆
┆       │        │      │ Node ├╸╸╸╸╸╸╸╸╸╸╸╸╸╸┤ Node │       │       │     ┆
┆       │        │      │      │ ┆ protocol ┆ │      │       │       │     ┆
┆      push     pull    │      │ ┆          ┆ │      │      pull    push   ┆
┆       │        │      │      │ ┆          ┆ │      │       │       │     ┆
┆       │        │      │      │ ┆          ┆ │      │       │       │     ┆
┆       │        │      │      │ ┆          ┆ │      │       │       │     ┆
┆  ┌────┴───┐ ┌──╽─────┐│      │ ┆          ┆ │      │ ┌─────╽──┐ ┌──┴────┐┆
┆  │working │ │working ││      │ ┆          ┆ │      │ │working │ │working│┆
┆  │copy    │ │copy    ││      │ ┆          ┆ │      │ │copy    │ │copy   │┆
┆  └────────┘ └────────┘└──────┘ ┆          ┆ └──────┘ └────────┘ └───────┘┆
└╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴┘          └╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴╴┘
```

On the original node, doing `git pull rad master` does not make much sense:

```
✓ Synced with 1 peer(s)
From rad://z3rw93C1rVFE9wvSPcXBTo8UT8i7a
 * branch            master     -> FETCH_HEAD
Already up to date.
```

Same thing for the second node:

```
✓ Synced with 1 peer(s)
Da rad://z3rw93C1rVFE9wvSPcXBTo8UT8i7a
 * branch            master     -> FETCH_HEAD
Già aggiornato.
```

However, you can directly pull data from the original node:

```
git pull vm@z6Mkrhm8eXtjbZK5J7Laz9fW5Z2dBcDZWRwtyGdapycvJZww master
✓ Synced with 1 peer(s)
Da rad://z3rw93C1rVFE9wvSPcXBTo8UT8i7a/z6Mkrhm8eXtjbZK5J7Laz9fW5Z2dBcDZWRwtyGdapycvJZww
 * branch            master     -> FETCH_HEAD
Già aggiornato.
```

## Conclusions

🔦 There are other features to explore such as the web interface, private
repositories, the `~/.radicle/config.json` file and repository deletion (don't
know if this is possible).
