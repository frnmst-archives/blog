---
title: Apache subpath rewrites
tags: [apache, rewrite, http, blog, post, notes, tutorial]
updated: 2022-10-08 17:40:00
description: Changing the subpath of some pages must be compensated by redirecting the old subpath to the new one to avoid broken backlinks
---

To have a more linear structure in my blog I changed the subpath of posts
which are now under `/post/` instead of `/notes/`.

The problem is that external websites which link pages of my blog ([backlinks](https://en.wikipedia.org/wiki/Backlink))
do not know about this change, so to avoid dead URLs I needed to redirect
requests from `/notes/\*.html` to `/post/\*.html`.

These instructions work for the Apache HTTPD server

1. login as root
2. enable the [rewrite module](https://httpd.apache.org/docs/current/mod/mod_rewrite.html)

   ```shell
   a2enmod rewrite
   ```

3. in your virtual host enable the `FollowSymLinks` option
4. the rewrite rule I use is like this

   ```apache
   RewriteRule ^notes/(.*\.html)$ https://blog.franco.net.eu.org/post/$1 [R,L]
   ```

   - notice the round brackets in `(.*\.html)`: those are required
   - `$1` correponds to the matched part in the source URL
   - `[R,L]` mean
     - `R`: apply external redirect
     - `L`: stop the rewriting rule after the current rule

## Example

Here is the full example of a virtual host

```apache
<VirtualHost *:443>

    # [ ... ]

    RewriteEngine on
    ServerName blog.franco.net.eu.org

    DocumentRoot "/var/www/blog.franco.net.eu.org"
    <Directory "/var/www/blog.franco.net.eu.org">
        Options -ExecCGI -Includes
        Options FollowSymLinks
        # No indexing.
        Options -Indexes

        #
        # AllowOverride controls what directives may be placed in .htaccess files.
        # It can be "All", "None", or any combination of the keywords:
        #   AllowOverride FileInfo AuthConfig Limit
        #
        AllowOverride None

        #
        # Controls who can get stuff from this server.
        #
        Require all granted

        RewriteRule ^notes/(.*\.html)$ https://blog.franco.net.eu.org/post/$1 [R,L]
    </Directory>

    # [ ... ]

</VirtualHost>
```
