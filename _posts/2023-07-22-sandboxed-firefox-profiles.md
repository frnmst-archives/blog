---
title: Sandboxed Firefox profiles
tags: [firefox, firejail, yad, browser, profile, sandbox, video, youtube]
related_links: ['https://youtu.be/vJ4CSaQ3cQs', 'https://youtube.com/shorts/8zHf-6Qt7ok']
updated: 2023-09-30 21:54:00
description: Manage sandboxed Firefox profiles using Firejail and an easy GUI interface to lauch them
canonical_url: 'https://youtu.be/vJ4CSaQ3cQs'
---

Let's see how to use sandboxed and isolated Firefox profiles using Firejail. YAD is used for the GUI menus.

<iframe width="560" height="315" src="https://www.youtube.com/embed/vJ4CSaQ3cQs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
