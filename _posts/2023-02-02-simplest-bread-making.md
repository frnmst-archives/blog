---
title: Simplest bread making
tags: [food, bread, recipe]
updated: 2023-02-15 17:08:00
description: ! 'A very simple recipe to make bread consisting in four ingredients'
images:
  - id: '0'
    filename: '0.jpg'
    alt: 'ingredients before mixing'
    caption: ! 'Ingredients before mixing'
    width: '300px'
  - id: '1'
    filename: '1.jpg'
    alt: 'spread dough'
    caption: ! 'Spread dough'
    width: '300px'
  - id: '2'
    filename: '2.jpg'
    alt: 'dough resting'
    caption: ! 'Dough resting'
    width: '300px'
  - id: '3'
    filename: '3.jpg'
    alt: 'final result'
    caption: ! 'Final result'
    width: '300px'
open_graph_protocol_default_image_id: '3'
---
<!--excerpt_start-->
Here is a simple recipe to make bread<!--excerpt_end-->.

Why? it's time to start *self-hosting* your food as well since the
EC has approved certain type insects for food consumption.

They say this[^1]:

> What was adopted by the Commission in January 2023?
> [ ... ]
> The novel food consists of the frozen, paste, dried and powder forms of house cricket. It is intended to be marketed as a food ingredient in a number of food products for the general population.
> [ ... ]
> Why should we eat insects?
> It is up to consumers to decide whether they want to eat insects or not. The use of insects as an alternate source of protein is not new and insects are regularly eaten in many parts of the world.
> [ ... ]

Yet another [Overton window](https://en.wikipedia.org/wiki/Overton_window)
is open. So, as long as you are certain that the flour (or anything else)
you are using is insect-free, you will be sure not to eat any of that stuff.

## Ingredients

| Name | Quantity | Required |
|------|----------|----------|
| Water | 350g    | ✓        |
| Flour | 300g    | ✓        |
| Baking soda | 1 tea spoon | ✓ |
| Salt | ½ tea spoon | ✓     |

## Tools

| Name | Required |
|------|----------|
| Oven | ✓ |
| Oven pan | ✓ |
| Parchment paper | ✓ |
| Non-powdered gloves (to do the mixing phase) | ✘ |
| Bowl | ✓ |
| Table | ✓ |
| Radiator (advised during winter) | ✘ |
| Pot lid{,s} | ✓ |
| Scale | ✓ |

## Steps

It takes about 80 minutes from zero to bread.

<ol>
<li>cover the pan with parchment paper</li>
<li>put all the ingredients together in the bowl and mix them with your hands.
    You have to get a uniform dough. Use the scale to measure the quantity
    of each ingredient

    {% include image.html id="0" %}
</li>
<li>spread the dough on the pan

    {% include image.html id="1" %}
</li>
<li>rest the dough for 30 minutes on the radiator or table. Put one or more
    pot lids on it, depending on the size of the dough.
    If you use parchment paper as top cover it will be much harder to separate
    it from the dough later on

    {% include image.html id="2" %}
</li>
<li>turn on the oven to 200°C without turning on the internal fans</li>
<li>once the resting time is over put the pan in the hot oven</li>
<li>leave the bread in the oven for about 30 minutes. Check once in a while.</li>
<li>remove the pan from the oven when the bread changes color from white to
    yellow/gold
</li>
<li>enjoy

   {% include image.html id="3" %}
</li>
</ol>

## Notes

[^1]: [approval insect novel food](https://food.ec.europa.eu/safety/novel-food/authorisations/approval-insect-novel-food_en), © European Union, 1995-2023, Creative Commons Attribution 4.0 International (CC BY 4.0) licence
