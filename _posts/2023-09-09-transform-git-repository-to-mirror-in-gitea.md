---
title: ! 'Transform GIT repository to mirror in Gitea'
tags: [gitea, git, mirror, git-mirror, adminer, database, tutorial, video, youtube]
updated: 2023-09-09 17:00:00
description: ! 'How to transform a standard GIT repository into a pull mirror in Gitea'
canonical_url: 'https://youtu.be/xE-D2y-p0V8'
---

Mirroring huge GIT repositories with Gitea is not always straight forward. Sometimes you get a timeout error or a connection problem that aborts the repository cloning. Although you can edit Gitea's mirroring timeout in its configuration, this is not always effective.

Another possibility is to gradually clone the repository by chunks, import it from Gitea and transform it to a mirror. This last part is the hacky part, since to do it you must make a couple of changes in the database. Do a backup first!

<iframe width="560" height="315" src="https://www.youtube.com/embed/xE-D2y-p0V8?si=DQyc3EYtlOK6q7zR" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
