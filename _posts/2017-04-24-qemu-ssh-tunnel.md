---
title: Qemu SSH tunnel
updated: 2021-12-29 18:23:28
tags: [tutorial, qemu, vnc, ssh-tunnel]
description: How to use qemu via VNC and SSH
---

Hello readers,

I was in need to run a virtual machine to do some experiments on `archiso`
(sic) and `parabolaiso`.

<!--more-->

My `Intel(R) Core(TM)2 Quad  CPU   Q8200  @
2.33GHz` PC desktop processor does not support virtualization technology which
means that when I run a virtual machine it's very, very slow (barely usable).
So I thought: why not use the home server which has newer hardware and
supports virtualization:

```shell
$ lscpu | grep Virtualization
Virtualization:        VT-x
```

*Please note that most steps described here have been integrated
in the qvm.py script. For this reason you should follow the
instructions reported in the
[documentation](https://docs.franco.net.eu.org/automated-tasks/scripts.html#qvm-py)*

Now, some time ago I've written a
[simple script](https://software.franco.net.eu.org/frnmst-archives/qvm)
to handle QEMU easily. VirtualBox is not available on Parabola and I don't
like the available QEMU frontends.

So, after about 1 hour of information
[retrieval](http://blog.scottlowe.org/2013/08/21/accessing-vnc-consoles-of-kvm-guests-via-ssh/)
[throughout](https://www.dragonsreach.it/2012/10/06/ssh-tunneling-for-vnc/)
[Internet](https://www.cyberciti.biz/faq/linux-kvm-vnc-for-guest-machine/)
[articles](https://www.cyberciti.biz/faq/howto-setup-vnc-server-ssh-client-tunnel-via-internet/)
I was able to connect all the dots... Here it is.

## Premise

The technique used here is called
["SSH tunneling"](https://en.wikipedia.org/wiki/Tunneling_protocol#Secure_Shell_tunneling)
and enables you to use an SSH server as an intermediary between the client and
a remote server. Let's see a trivial scheme of the VNC setup

```
            port 22               port 5900
    client <--> server.ssh_interface <--> server.vnc_interface
      ^
      |
      |
      | port 5901
      |
    $ vncviewer 127.0.0.1:1 (:1 is equivalent to :5901)
```

## Terminology

- `<-->`: connection or forwarding
- `127.0.0.1`: local address (a.k.a `localhost`)
- `server.address`: address of the server
- `server-user`: login name of the server
- `vm-user`: login name of the virual machine

Note: `server.vnc_interface` is in a localhost location in respect to the
`server.ssh_interface`, so we will not use the term *intermediary server*
anymore.

## Server

First thing: since my server does not have a GUI I installed the
`qemu-headless` package.

This line does the magic:
```shell
-monitor pty -vnc 127.0.0.1:0
```

Make sure to have the following configurations in the OpenSSH configuration
(`/etc/ssh/sshd_config`) otherwise the next steps won't work

    AllowTcpForwarding yes

In case you don't, you must also restart the SSH daemon.

You can now use the appropriate QVM vnc command.

## Client

Download the QVM script on the client also.

You must now install one of the vnc clients like
[gtk-vnc](https://wiki.gnome.org/Projects/gtk-vnc) or
[TigerVNC](http://www.tigervnc.org).
I've noticed that TigerVNC seems to handle window resizes better, so I decided
to go for that one.

Before starting the VNC client, an SSH socket (tunnel) is created.

```shell
$ ssh -N -f -L 5901:127.0.0.1:5900 server-user@server.address
```
TigerVNC is then called on the forwarded port.

```shell
$ vncviewer 127.0.0.1:1
```

You should now see the virtual machine.

## SSH

The next thing was to connect to the SSH daemon on the virtual machine just
like what qvm enables you to do. I thought I could use the same method of VNC.

Once the SSH daemon is up and running you can connect to it with the following
command from the server:

```shell
$ ssh -p 2222 vm-user@127.0.0.1
```

The SSH port of the virual machine is the default one (22). qvm exposes the
port 2222 by default so you can connect from `localhost` with it.

We need another step to be able to connect remotely and directly to the virtual
machine from our client:

```shell
$ ssh -N -f -L 2223:127.0.0.1:2222 server-user@server.address
```

then

```shell
$ ssh -p 2223 vm-user@127.0.0.1
```

You should now see the login.

### A simpler way to connect through SSH

As I later found out, it is possible to connect to SSH, as well as any ohter
service, by simply using the host address and the forwarded port, for example:

```shell
$ ssh -p 2222 vm-user@server.address
```

This happens because with this configuration the guest network is bridged with
the host network.

## Final considerations

You can use this method also for internet browsing and lots of other stuff.
Infact, using SSH implies that the traffic between the client and remote server
is encrypted, but using VNC directly by default is NOT so pay
attention.

Cheers!
