---
title: A new fan for the processor
updated: 2021-09-13 14:07:00
tags: [tutorial, fan, cpu]
description: 'A new fan for the processor'
images:
  - id: '0'
    filename: '0.jpg'
    alt: 'box 0'
    caption: ! 'The fan box'
  - id: '1'
    filename: '1.jpg'
    alt: 'box 1'
    caption: ! 'Technical information reported on the box'
  - id: '2'
    filename: '2.jpg'
    alt: 'box 2'
    caption: ! 'The back of the box'
  - id: '3'
    filename: '3.jpg'
    alt: 'unboxing 0'
    caption: ! 'Unboxing'
  - id: '4'
    filename: '4.jpg'
    alt: ! 'unboxing 1'
  - id: '5'
    filename: '5.jpg'
    alt: ! 'unboxing 2'
  - id: '6'
    filename: '6.jpg'
    alt: ! 'unboxing 3'
  - id: '8'
    filename: '8.jpg'
    alt: 'test 0'
    caption: ! 'Test and comparison with old heatsink before assembly. You may already see a problem here :)'
  - id: '10'
    filename: '10.jpg'
    alt: 'test 1'
  - id: '11'
    filename: '11.jpg'
    alt: 'test 2'
  - id: '12'
    filename: '12.jpg'
    alt: 'test 3'
  - id: '13'
    filename: '13.jpg'
    alt: 'cabling 0'
  - id: '14'
    filename: '14.jpg'
    alt: 'cabling 1'
  - id: '15'
    filename: '15.jpg'
    alt: 'cabling 2'
  - id: '16'
    filename: '16.jpg'
    alt: 'cabling 3'
  - id: '17'
    filename: '17.jpg'
    alt: 'cabling 4'
  - id: '18'
    filename: '18.jpg'
    alt: 'cabling 5'
  - id: '19'
    filename: '19.jpg'
    alt: 'cabling 6'
  - id: '21'
    filename: '21.jpg'
    alt: 'motherboard 0'
    caption: ! 'Mother board with the heatsink mounted'
  - id: '22'
    filename: '22.jpg'
    alt: 'motherboard 1'
    caption: ! 'Everything back in place. Ready to test'
  - id: '28'
    filename: '28.jpg'
    alt: 'case 0'
    caption: ! "Case mods or it won't fit"
  - id: '30'
    filename: '30.jpg'
    alt: 'case 1'
  - id: 'fc'
    filename: 'fan_control_report.png'
    alt: 'Fan report'
  - id: '23'
    filename: '23.jpg'
    alt: 'bios setings 0'
    caption: ! '((2650 / 2333 MHz) * 100) - 100 = + 13.59% clocking'
  - id: '24'
    filename: '24.jpg'
    alt: 'bios settings 1'
    caption: ! '380 * 7 MHz = 2660 MHz. Maybe too much?'
  - id: '26'
    filename: '26.jpg'
    alt: 'bios settings 2'
    caption: ! "If I don't increase the CPU voltage the BIOS screen won't even come up and the BIOS settings need to be cleared manually"
  - id: '27'
    filename: '27.jpg'
    alt: 'kernel panic'
    caption: "Here's what happens"
---

Hello again,

Some time ago I upgraded the CPU fan because I noticed that the stock one
was broken.

Here's the whole story.

<!--more-->

A couple of years ago I put in a cheap case fan retrieved from a 2003
computer just to keep things cooler. The problem of this
[small fan](https://encrypted.google.com/search?hl=en&q=smaller%20fan%20more%20noise)
was the tremendous noise which I couldn't bare anymore.

So, some months ago I decided to open the computer and I was
surprised in discovering that the CPU fan wasn't working. Now I had two
problems: the noise and one broken fan. I took drastic measures: I pulled out
the noisy fan and the broken fan. Then I tested the CPU temperatures using `$
sensors` from the `lm_sensors` package. I found out that the temperatures got
up to about 80 °C which is unacceptable for a CPU. I needed to try something
else.

I then went into the BIOS options and I heavily undercloked the CPU to obtain
temperatures like these (however I don't remember the exact clock values):

```shell
[parabola@ppc ~]$ sensors
temp2:        +50.0°C  (high = +80.0°C, hyst = +75.0°C)
temp3:        +46.5°C  (high = +80.0°C, hyst = +75.0°C)
Core 0:       +69.0°C  (high = +80.0°C, crit = +100.0°C)
Core 1:       +63.0°C  (high = +80.0°C, crit = +100.0°C)
Core 2:       +66.0°C  (high = +80.0°C, crit = +100.0°C)
Core 3:       +66.0°C  (high = +80.0°C, crit = +100.0°C)
```

I went on like this for months but, as you can imagine, the computer was a bit
slow and laggy especially using Iceweasel.

It was time to buy a new fan and heat sink. So I bought a [Cooler Master Hyper
212X](https://www.amazon.it/Cooler-Master-Hyper-212X-Ventola/dp/B01ARGVNV6)
for about 30 euros (which seems to be a very reasonable price). This
choice was made because of various factors (in decreasing order of importance):

- The compatibility with my [`LGA 775`](https://en.wikipedia.org/wiki/LGA_775)
  motherboard
- The price
- The reiews
- Noise information declared
- Thermal paste included (which saves some euros from shipping)

Once I got the fan the hard part arrived: I had to remove the motherboard from
the case to work comfy. This was the first time for me but it well better than
I expected, except for a minor irritation at the end.

{% include image.html id="0" %}
{% include image.html id="1" %}
{% include image.html id="2" %}
{% include image.html id="3" %}
{% include image.html id="4" %}
{% include image.html id="5" %}
{% include image.html id="6" %}
{% include image.html id="8" %}
{% include image.html id="10" %}
{% include image.html id="11" %}
{% include image.html id="12" %}

I took some pictures to rember how the cabling should be.

{% include image.html id="13" %}
{% include image.html id="14" %}
{% include image.html id="15" %}
{% include image.html id="16" %}
{% include image.html id="17" %}
{% include image.html id="18" %}
{% include image.html id="19" %}
{% include image.html id="21" %}
{% include image.html id="22" %}
{% include image.html id="28" %}
{% include image.html id="30" %}

I didn't take exact measurements so I knew there was that risk. Since it's an
old PC I don't really care about the case. If you have a new PC i suggest you
take measurements to see if a potential new internal component fits before you
buy it.

At this stage I powered up the computer. The new fan seemed to work fine so I
removed the undercloking BIOS settings.

## Use of fancontrol

Finally I configured fancontrol to control automatic throttling of the fan
based on the CPU temperature. I used the CLI tool called `pwmconfig` in manual
mode otherwise it wouldn't detect the fan. Once the tool finished it generated
a configuration file and the gnuplot chart below. This chart is not really
useful to us because it correlates RPMs with PWMs instead of RPMs and
temperature. It just shows how the computer will handle the fan to get it to a
certain speed.

{% include image.html id="fc" %}

Here is the new `sensors` information

```
w83627dhg-isa-0290
Adapter: ISA adapter
fan2:        1506 RPM  (min = 33750 RPM, div = 8)
temp1:        +32.0°C  (high = +25.0°C, hyst = +31.0°C)
temp2:        +34.0°C  (high = +80.0°C, hyst = +75.0°C)
temp3:        +44.5°C  (high = +80.0°C, hyst = +75.0°C)
Core 0:       +55.0°C  (high = +80.0°C, crit = +100.0°C)
Core 1:       +50.0°C  (high = +80.0°C, crit = +100.0°C)
Core 2:       +52.0°C  (high = +80.0°C, crit = +100.0°C)
Core 3:       +52.0°C  (high = +80.0°C, crit = +100.0°C)
```

Much better!

The final thing to do was to add `fancontrol` as a system service.

## Overclocking?

Is it a good idea to overclock now that temperature is not a problem anymore?
In my case the answer is no.

{% include image.html id="23" %}
{% include image.html id="24" %}
{% include image.html id="26" %}
{% include image.html id="27" %}

Even at 10% (+10% means 110% of overall clock)
I had kernel panics sooner or later, although I could see it was faster. This
is probably due to ~~the power supply which is not able to stand more electric
consumption~~ the combination of processor and motherboard. According to
[some](http://www.tomshardware.co.uk/forum/283888-29-overclock-q8200-778ghz)
[websites](http://www.hardwarecanucks.com/forum/overclocking-tweaking-benchmarking/60254-overclocking-q8200-asrock-motherboard.html)
neither the processor nor the motherboard were made for
this. So I gave up overclocking. Too bad...

Till next time.
