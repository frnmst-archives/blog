---
title: Repairing my first PC
tags: [retrocomputer, hardware]
updated: 2018-12-02 23:37
description: Attempt to repair my first PC and part inventory
images:
  - id: 'dsl'
    filename: 'dsl_boot_on_old_pc.gif'
    alt: 'booting Damn Small Linux (DSL) from the CD-ROM'
    caption: 'booting Damn Small Linux (DSL) with the CD-ROM'
  - id: 'mb'
    filename: 'DSCN3132.JPG'
    alt: 'motherboard and processor'
  - id: 'scvc'
    filename: 'DSCN3131.JPG'
    alt: 'sound card and video card'
  - id: 'hdd0'
    filename: 'DSCN3156.JPG'
    alt: 'HDD'
  - id: 'hdd1'
    filename: 'DSCN3157.JPG'
    alt: 'HDD'
  - id: 'hdd2'
    filename: 'DSCN3159.JPG'
    alt: 'HDD'
  - id: 'fddpw'
    filename: 'DSCN3130.JPG'
    alt: 'FDD and power supply'
  - id: 'kb'
    filename: 'DSCN3134.JPG'
    alt: 'Keyboard and case connectors'
  - id: 'mon0'
    filename: 'DSCN3140.JPG'
    alt: 'monitor back view'
  - id: 'mon1'
    filename: 'DSCN3143.JPG'
    alt: 'monitor front view'
---

This was my first PC back in ~ 2001 2002. It was already used back then and
it shipped with the mighty Micro$oft Windows 98 OS (probably the "First
Edition").

{% include image.html id="dsl" %}

I remember using *Intern€t Explor€r* (version 5?) with
this thing (sic) and to do that I had to connect the internal
[Winmodem](https://en.wikipedia.org/wiki/Softmodem) to a
[tripolar phone
plug](https://en.wikipedia.org/wiki/Tripolar_plug) with an RJ-45 to
tripolar beige wire (this also happened with the
next computer). Internet speed was around 32Kbps on average.
No USBs, no WiFi, no Ethernet. Only a CD-ROM and a floppy disk drive which was
the only medium I used at the time to move files. Infact I remember using
floppy drives for school researches using Micro$oft Word as an editor.

It was not a gaming PC but I was able to play
titles like *Hard Truck: Road To Victory*, *Age Of Empires II* or *Flight
Simulator 98* anyway.

Sadly, some years ago this PC refused to boot up. This happened
more and more frequenly and I had no idea on what was going on. I gave up
using it...

... until I decided to repair it and I noticed some corrosion on
the RAM module. I found [this post](http://www.tomshardware.co.uk/forum/253501-30-cleaning-memory-eraser-chips)
which suggests using a pencil eraser on the pins. It worked. When booting, the
leds of the AT keyboard instead of blinking once, now blinked twice. So I
connected the video card as well but nothing happened. I then connected a PC
buzzer and found out that it was an
[AGP socket problem](http://www.helpwithpcs.com/upgrading/post-beep-codes.htm) a.k.a.
*Video error*, since the motherboard has an AWARD BIOS.

I tried another AGP video card with no luck. The AGP socket was probably just
broken.

To be sure the system was working I tried a Windows 98 recovery boot floppy
disk. I issued the `dir` command and listened to the floppy drive making
the usual noise.

Since it's pretty useless as it is, and of course painly slow, I think it's
time to recycle the components...

What follows is a thorough inventory of the parts with some photos.

## The full hardware specs

### Motherboard

- 1x Unkown "Super Socket 7" motherboard with AWARD BIOS and VIA, Winbond and
  other chips.

{% include image.html id="mb" %}

#### Slots

- 3x PCI
- 2x ISA
- 2x DIMM 168 pin
- 2x [SIMM](https://en.wikipedia.org/wiki/SIMM) RAM (Don't know pin #)

#### Connectors

- 1x ATA Floppy
- 1x primary IDE (ATA)
- 1x secondary IDE (ATA)
- 1x PS/2 mouse (not the actual PS/2 connector but 5 pins. See [this](https://www.altex.com/5-Pin-to-6-Pin-F-Intel-PS2-Mouse-Cable-BKT-PS2-P139177.aspx) for example)
- 2x COM 10 pin
- 1x IDC 26 pin
- 1x USB 8 pin (don't know what version but i suspect it's 1.1)
- 1x 3 pin fan
- 1x AT Keyboard
- 1x AGP 124
- 1x P8 P9 (power)
- 1x 20 pix ATX (power)
- 1x KEYLOCK
- 1x 4 pin speaker (buzzer)
- 1x reset
- 1x HDD LED
- 1x power ON/OFF
- 1x `EXTSMI`
- 1x INFRA-RED

### Processor

- 1x AMD `AMD-K6-2/450AFX`

### Cards

- 1x PCI Winmodem (can't find it; I think it was a Lucent one)
- 1x ISA `CRYSTAL CX4235-XQ3` sound card
- 1x `ATI HIS Rage IIc` video card

{% include image.html id="scvc" %}

### Memory

- 32MB `DIMM32MTEC AL0457BY` DIMM RAM. It has 4 `M.tec` memory modules on the
  front.

### Drives

- 1x 1.44 MB `MITSUMI MODEL D359M3` floppy drive
- 1x 44x `Delta Electronics, Model OIP-CD440A` CD-ROM drive
- 1x 8.4GB `Seagate U8 Model ST38410A` HDD

{% include image.html id="hdd0" %}
{% include image.html id="hdd1" %}
{% include image.html id="hdd2" %}

### Power Supply

- Power supply with P8 and P9 pins: `EXON COMPUTER SWITCHING POWER SUPPLY
  230W`. Model name should be `EX04T`.

{% include image.html id="fddpw" %}

### Case Connectors

These connectors are placed on the back part of the chassis. They have the
classic bracket form factor.

#### Connector 1

- 1x female PS/2 mouse connector
- 1x female parallel port

#### Connector 2

- 1x 9 pin male serial port
- 1x 25 pin male serial port

{% include image.html id="kb" %}

### Internal accessories

- A [4 pin audio cable](https://encyclopedia2.thefreedictionary.com/CD-ROM+audio+cable)
  that connected the CD-ROM drive with the sound card (can't find it). The wire
  was grey and the two connectors were black.

### Case

- The case was a classical beige 1990s looking one, with a single metal cover
  around the left, right and top sides.

### External Accessories

- AT Monterey Keyboard `MODEL : K280W` with the right foot/leg broken
  (looking at the keyboard from behind).
- DELL Trinitron monitor `UltraScan 800HS Series MODEL: D825TM`, with a nice
  integrated crack on the basement and a non functional horizontal line on the screen.
- A serial mouse (can't find it)
- Classical PC speakers (light blue transparent plastic) with power supply (can't find them)

{% include image.html id="mon0" %}
{% include image.html id="mon1" %}
