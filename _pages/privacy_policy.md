---
layout: page
title: Privacy policy
permalink: /privacy-policy/
description: Privacy policy of blog.franco.net.eu.org
---

The same rules for [software.franco.net.eu.org](https://software.franco.net.eu.org)
apply for this blog. Have a look at the
[*Privacy policy*](https://software.franco.net.eu.org/frnmst/software.franco.net.eu.org/src/branch/master/privacy_policy.md#privacy-policy)
paragraph.

No cookies are used.
