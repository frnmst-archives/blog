---
layout: page
title: Jobs
permalink: /jobs/
description: My computer science jobs and gigs
---

## Fiverr gigs

- [I will add accurate subtitles to your video in english or italian](https://fiverr.com/s/VgeQGl)
- [I will write API Python Sphinx documentation](https://fiverr.com/s/PNx8rE)
- [I will write python setup files](https://fiverr.com/s/jEBQQo)

[Contacts]({{- '/about/' | relative_url -}}#contacts)

## Lessions

I give lessions in computer science (programming, database, networks, Linux, etc...)
via video call on Nextcloud or Skype.

[Contacts]({{- '/about/' | relative_url -}}#contacts)

## Other

- Linux installations
- Android ROM installations
- Generic PC problem resulutions
- Data recovery (if the memory is not physically damaged, for example if the
  user deleted files involuntarily

[Contacts]({{- '/about/' | relative_url -}}#contacts)
