---
layout: default
permalink: index.html
---

# {{ site.title }}

{% include tag_list.html %}
<div class="index" markdown="0">
<hr>
<ul class="post-list" itemscope itemtype="http://schema.org/Blog">
{%- for post in site.posts -%}
    <li itemprop="blogPost" itemscope itemtype="http://schema.org/BlogPosting">
{%- comment -%}See http://frontendcollisionblog.com/jekyll/snippet/2015/03/23/how-to-show-a-summary-of-your-post-with-jekyll.html
             which is released under the MIT license, Copyright (c) 2015 Joshua Beam
{%- endcomment -%}
{%- capture post_excerpt -%}
{%- if post.content contains site.excerpts.start and post.content contains site.excerpts.end -%}
    {%- assign start=post.content | split: site.excerpts.start | last -%}
    {%- assign end=start | split: site.excerpts.end | first -%}
{{- end | strip_html -}}...
{%- else -%}
{{- post.content | strip_html | truncatewords: site.excerpts.words -}}
{%- endif -%}
{%- endcapture -%}
        <h4>
            <a class="post-link" href="{{ post.url | prepend: site.rooturl }}" title="{{ post.title | escape }}" itemprop="url"><span itemprop="name">{{ post.title }}</span></a>
        </h4>
<span class="post-meta"><time itemprop="datePublished" datetime="{{ post.date | date_to_xmlschema }}">{{ post.date | date: '%F' }}</time></span>
<div itemprop="description">{{ post_excerpt -}}</div>
    </li>
{%- endfor -%}
</ul>
</div>
