---
layout: page
title: Sitemap
permalink: /sitemap/
description: Sitemap of blog.franco.net.eu.org
---

{% assign site_pages = site.pages | sort %}
### Pages
<ul>
{%- for page in site_pages -%}
    {%- if page.title != 404 -%}
<li>
<a href="{{- page.url | prepend: site.rooturl -}}">{{- page.title -}}</a>
</li>
    {%- endif -%}
{%- endfor -%}
</ul>

{%- if site.posts -%}
    {%- assign site_posts = site.posts | sort -%}
### Posts
<ul>
    {%- for post in site_posts -%}
<li>
<a href="{{- post.url | prepend: site.rooturl -}}">{{- post.title -}}</a>
</li>
    {%- endfor -%}
</ul>
{%- endif -%}
