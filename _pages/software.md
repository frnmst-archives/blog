---
layout: page
title: Software
permalink: /software/
description: Official source for software published by Franco Masotti
---

## Table of contents

{% comment -%}O(sorting algorithm){%- endcomment -%}
{%- assign ss = site.software | uniq | sort_natural: 'software_version_raw' | reverse -%}
{% assign software_names = "" | split: ',' -%}
{%- comment -%}
Filter meta files only.

O(n) where n == len(files with front matter in ./_software)
{%- endcomment -%}
{%- for p in site.software -%}
    {%- if p.meta -%}
        {%- assign software_names = software_names | push: p.software_name -%}
    {%- endif -%}
{%- endfor -%}

{%- comment -%}Markdown TOC must be generated manually because the content of this page is dynamic.{%- endcomment -%}
<ul>
<li><a href="#table-of-contents">Table of contents</a></li>
<li><a href="#introduction">Introduction</a></li>
<li><a href="#software">Software</a></li>
<ul>
{%- assign i=0 -%}
{%- assign j=0 -%}
{%- for s in software_names -%}
  <li><a href="#{{ s | slugify }}">{{ s }}</a></li>
    <ul>
    {%- if i == 0 -%}
    <li><a href="#repository">Repository</a></li>
    <li><a href="#documentation">Documentation</a></li>
    {%- for meta in ss -%}
        {%- if meta.meta and s == meta.software_name -%}
            {%- if meta.related_links -%}
    <li><a href="#related-links">Related links</a></li>
                {%- assign j = j | plus: 1 -%}
            {%- endif -%}
        {% endif %}
    {% endfor %}
    <li><a href="#releases">Releases</a></li>
    {%- else -%}
    <li><a href="#repository-{{ i }}">Repository</a></li>
    <li><a href="#documentation-{{ i }}">Documentation</a></li>
    {%- for meta in ss -%}
        {%- if meta.meta and s == meta.software_name -%}
            {%- if meta.related_links -%}
                {%- if j > 0 %}
    <li><a href="#related-links-{{ j }}">Related links</a></li>
                {% else %}
    <li><a href="#related-links">Related links</a></li>
                {% endif %}
                {%- assign j = j | plus: 1 -%}
            {%- endif -%}
        {% endif %}
    {% endfor %}
    <li><a href="#releases-{{ i }}">Releases</a></li>
    {%- endif -%}
    </ul>
  {%- assign i = i | plus: 1 -%}
{%- endfor -%}
</ul>
</ul>

{%- comment -%}
THIS IS BAD:
O(m * (n**2)) = O(n**3)
where
    m = len(software_names)
    n = len(files with front matter in ./_software)
{%- endcomment -%}
<div markdown="1">

## Introduction

This page is the only *real* trusted source of some of my software, publicly available on the Internet.

## Software

<li><a href="{{- site.rooturl -}}/software/instructions">instructions</a></li>

{%- for software in software_names -%}
    {%- assign i=0 -%}
    {%- for p in ss -%}
        {%- if p.software_name == software and p.software_version -%}
            {% if i == 0 %}
### {{ p.software_name }}
                {%- comment -%}No checks for duplicates. It is your responsability.{%- endcomment -%}
                {%- for meta in ss -%}
                    {%- if meta.meta and p.software_name == meta.software_name -%}
                        {%- if meta.archived %}
*Note: this repository is archived*
                        {% endif %}

#### Repository

<ul>
                                {%- if meta.canonical_repository_url -%}
<li><a href="{{ meta.canonical_repository_url }}">canonical repository</a></li>
                                {%- endif -%}
                                {%- if meta.codeberg_repository_url -%}
<li><a href="{{ meta.codeberg_repository_url }}">Codeberg</a></li>
                                {%- endif -%}
                                {%- if meta.framagit_repository_url -%}
<li><a href="{{ meta.framagit_repository_url }}">Framagit</a></li>
                                {%- endif -%}
                                {%- if meta.github_repository_url -%}
<li><a href="{{ meta.github_repository_url }}">GitHub</a></li>
                                {%- endif -%}
</ul>

#### Documentation

<ul>
                                {%- if meta.documentation_url and meta.documentation_ref -%}
<li><a href="{{- meta.documentation_url -}}">{{- meta.documentation_ref -}}</a></li>
                                {%- endif -%}
                                {%- if meta.has_changelog -%}
<li><a href="{{- site.rooturl -}}/software/CHANGELOG-{{- meta.software_name -}}.html">changelog</a></li>
                                {%- endif -%}
</ul>
                                {% if meta.related_links %}
#### Related links
<ul>
                                    {% for l in meta.related_links %}
<li><a href="{{ l.url }}">{{ l.label }}</a></li>
                                    {% endfor %}
</ul>
                                {% endif %}
                    {%- endif -%}
                {%- endfor %}
#### Releases

<ul>
                {%- assign i = i | plus: 1 -%}
            {%- endif -%}
<li><a href="{{ site.rooturl }}/software/{{ p.software_name }}-{{ p.software_version }}/release.html"><code>{{ p.software_version }}</code></a></li>
        {%- endif -%}
    {%- endfor -%}
</ul>
{%- endfor -%}
</div>
