---
layout: page
title: Software instructions
permalink: /software/instructions/
description: Instructions for cryptographical signing, verification, releasing and integrity of software released by Franco Masotti
---

## Table of contents

<!--TOC-->

- [Table of contents](#table-of-contents)
- [Introduction](#introduction)
- [Extract](#extract)
- [Signing keys](#signing-keys)
- [Terminology](#terminology)
- [Methods](#methods)
  - [Upload (what I have to do)](#upload-what-i-have-to-do)
  - [Download (what you have to do)](#download-what-you-have-to-do)

<!--TOC-->

## Introduction

Here you will find methods to assert the authenticity of the presented software packages.

## Extract

The following extract is from a [post by Mike Gerwitz](https://mikegerwitz.com/2012/05/a-git-horror-story-repository-integrity-with-signed-commits#trust):

> Git Host
>>
>>    Git hosting providers are probably the most easily overlooked trustees—providers like Gitorious, GitHub, Bitbucket, SourceForge, Google Code, etc. Each provides hosting for your repository and “secures” it by allowing only you, or other authorized users, to push to it, often with the use of SSH keys tied to an account. By using a host as the primary holder of your repository—the repository from which most clone and push to—you are entrusting them with the entirety of your project; you are stating, “Yes, I trust that my source code is safe with you and will not be tampered with”. This is a dangerous assumption. Do you trust that your host properly secures your account information? Furthermore, bugs exist in all but the most trivial pieces of software, so what is to say that there is not a vulnerability just waiting to be exploited in your host’s system, completely compromising your repository?
>>
>>    It was not too long ago (March 4th, 2012) that a public key security vulnerability at GitHub was exploited by a Russian man named Egor Homakov, allowing him to successfully commit to the master branch of the Ruby on Rails framework repository hosted on GitHub. Oops.
>
> Copyright © 2019 Mike Gerwitz. Licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.

## Signing keys

You may contact me directly to obtain the public key fingerprint in a different way.

| Key | Fingerprint |
|-----|-------------|
| [pgp_pubkey_since_2019.txt]({{- site.rooturl -}}/pubkeys/pgp_pubkey_since_2019.txt) | [pgp_pubkey_fingerprint_since_2019.txt]({{- site.rooturl -}}/pubkeys/pgp_pubkey_fingerprint_since_2019.txt) |

## Terminology

Commands are to be run in the project directory.

| Variable | Description | Command |
|----------|-------------|---------|
| project_dir | the full path directory of the project | `export project_dir="$(pwd)"` |
| project | the project name | `export project="$(basename "$(pwd)")"` |
| project_python_module | the python module name of the project. For example: `md-toc` is `md_toc` | `export project_python_module="$(basename "$(pwd)" | tr '-' '_')"` |
| project_version_release_timestamp | the timestamp of a software version (tag) in UTC format | `export project_version_release_timestamp="$(python3 -c 'from dateutil import parser as dateutil_parser; from dateutil.tz import UTC; import sys; p = dateutil_parser.parse(sys.argv[1]); u=p.astimezone(UTC); print(u.strftime("%Y-%m-%d %T"))' "$(git tag -l --format='%(taggerdate)' $(git describe --tags $(git rev-list --tags --max-count=1)))")"` |
| tag | the git tag name which is usually [semver](https://semver.org/)ed | `export tag="$(git describe --tags $(git rev-list --tags --max-count=1))"` |
| tag_raw | same as `tag` but pad each component of the tag with 6 zeros. For example: `12.121.5` becomes `000012.000121.000005` | - |
| signing_key | the public key file used to sign the archive file | - |
| has_changelog | the project has a changelog entry for a specific release. Value must be either `true` or `false` | - |
| is_on_pypi | the project is on PyPI. Value must be either `true` or `false` | - |
| changelog_slugified_header | the slugified header corresponding to a tag in a changelog file | - |
| url | a generic url | - |
| pypi_download_page | the URL of the download page of the package on PyPI | - |

## Methods

### Upload (what I have to do)

What follows are the steps I use to upload the software page.

1. create an archive

   ```shell
   cd /tmp
   git -C ${project_dir} archive --format=tar.gz --output=/tmp/${project}-${tag}.tar.gz --prefix=${project}-${tag}/ ${tag}
   ```

2. sign the archive

   ```shell
   gpg --armor --output ${project}-${tag}.tar.gz.sig --detach-sig ${project}-${tag}.tar.gz
   ```

3. get the checksums

   ```shell
   sha512sum ${project}-${tag}.tar.gz > ${project}-${tag}.tar.gz.SHA512SUM.txt
   sha256sum ${project}-${tag}.tar.gz > ${project}-${tag}.tar.gz.SHA256SUM.txt
   ```

4. if the project is on PyPI

   ```shell
   make dist
   cd dist
   sha256sum ${project_python_module}-${tag}-py3-none-any.whl > ${project_python_module}-${tag}-py3-none-any.whl.SHA256SUM.txt
   md5sum ${project_python_module}-${tag}-py3-none-any.whl > ${project_python_module}-${tag}-py3-none-any.whl.MD5SUM.txt
   ```

5. create a new release file called `_software/${project}-${tag}/release.md`
   and add the following. If it is not a Python project
   you must omit the `software_name_python_module` variable.
   Add the `### Added`, `### Removed`, etc... (changelog) contents if applicable.

   ```yaml
   ---
   layout: software_release
   enable_markdown: true
   title: release
   excerpt: none
   tags: [${csv_list}]
   software_name: ${project}
   software_name_python_module: ${project_python_module}
   software_version: ${tag}
   software_version_raw: ${tag_raw}
   release_timestamp: ${project_version_release_timestamp}
   is_on_pypi: ${is_on_pypi}
   has_changelog: ${has_changelog}
   signing_public_key: ${signing_key}
   ---

   ### Added

   - a
   - b
   - c

   ### Removed

   ...
   ```

### Download (what you have to do)

Run the following to download and verify the software.

1. if the public key is unknown you must import it from a trusted source

   ```shell
   cd /tmp
   wget "${public_key_url}"
   gpg --import "${public_key_file}"
   ```

2. download the repository

   ```shell
   cd /tmp
   wget ${url}/${project}-${tag}.tar.gz.sig
   ```

3. check the signature

   ```shell
   wget ${url}/${project}-${tag}.tar.gz
   gpg --verify ${project}-${tag}.tar.gz.sig
   ```

4. run the checksums

   ```shell
   sha512sum --check ${project}-${tag}.tar.gz.SHA512SUM.txt
   sha256sum --check ${project}-${tag}.tar.gz.SHA256SUM.txt
   ```

5. extract

   ```shell
   tar -xvzf ${project}-${tag}.tar.gz
   ```

6. if it is a Python project on PyPI

   ```shell
   wget ${pypi_download_page}/${project_python_module}-${tag}-py3-none-any.whl
   sha256sum --check ${project_python_module}-${tag}-py3-none-any.whl.SHA256SUM.txt
   md5sum --check ${project_python_module}-${tag}-py3-none-any.whl.MD5SUM.txt
   ```
